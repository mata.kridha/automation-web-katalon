<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Events        Akses berbagai materi dan_c82c0c</name>
   <tag></tag>
   <elementGuidId>2f0a2274-780c-4cce-94d5-c084f597cb73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div[6]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4f934ce9-36d1-4ff0-9f04-e6259b8eac18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>online-course-block</value>
      <webElementGuid>d8b3d5bd-7663-4046-a362-f9bd293373b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Events
        Akses berbagai materi dan sertifikat.
            Gratis
            ikut semua webinar untuk peserta bootcamp.
        

        
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 3: Predict using Machine Learning
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                18 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 18 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 4: Workshop
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                25 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 25 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 2: Data Wrangling with Python
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                11 Nov 2022,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            CLOSE
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 1: Introduction to Python for Data Scientist
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                04 Nov 2022,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            CLOSE
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                    

        
            Lihat Semua Event
        
    </value>
      <webElementGuid>8a7b2b80-1f10-4fd3-b3b0-0a2704f74598</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;online-course-block&quot;]</value>
      <webElementGuid>4e82d58f-f054-4bf9-90f2-a092a67b5674</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[6]</value>
      <webElementGuid>8f462cd6-7503-4ae6-8050-dc070bb27450</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Semua Course'])[1]/following::div[1]</value>
      <webElementGuid>224d3892-631d-4de2-b0c1-4e7d33c0c803</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Starter to Express JS API'])[1]/following::div[1]</value>
      <webElementGuid>ef8bf183-92f6-4030-9e59-6b623509f2e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[6]</value>
      <webElementGuid>1fc94343-fd26-42e8-8500-01df7c63ad76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Events
        Akses berbagai materi dan sertifikat.
            Gratis
            ikut semua webinar untuk peserta bootcamp.
        

        
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 3: Predict using Machine Learning
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                18 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 18 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 4: Workshop
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                25 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 25 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 2: Data Wrangling with Python
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                11 Nov 2022,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            CLOSE
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 1: Introduction to Python for Data Scientist
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                04 Nov 2022,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            CLOSE
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                    

        
            Lihat Semua Event
        
    ' or . = '
        Events
        Akses berbagai materi dan sertifikat.
            Gratis
            ikut semua webinar untuk peserta bootcamp.
        

        
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 3: Predict using Machine Learning
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                18 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 18 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 4: Workshop
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                25 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 25 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 2: Data Wrangling with Python
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                11 Nov 2022,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            CLOSE
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                            
                    
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 1: Introduction to Python for Data Scientist
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                04 Nov 2022,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            CLOSE
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    
                
                    

        
            Lihat Semua Event
        
    ')]</value>
      <webElementGuid>69334a8e-808e-4c90-8757-508d87b67746</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
