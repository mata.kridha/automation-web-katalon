<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Isi data diri dulu yuk,                  _01691a</name>
   <tag></tag>
   <elementGuidId>9f41bfea-cb33-4273-9856-d818405926e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>592de3c4-480d-4042-b3a8-76a7db20b26c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-show</name>
      <type>Main</type>
      <value>!$store.dataForm.toDahsboard</value>
      <webElementGuid>09280d59-b30b-4df6-89c1-276a89103c4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body1_regular</value>
      <webElementGuid>8ee8b11d-3336-4439-95d5-ac5658e7a6ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.</value>
      <webElementGuid>d85d2115-dc54-450c-9271-9a4a85f7c864</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/p[@class=&quot;new_body1_regular&quot;]</value>
      <webElementGuid>ea3c04a3-a6c9-4ce2-b2c2-38c43c37d1f9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/p</value>
      <webElementGuid>0eb44638-e8e7-4bc2-a42f-fb0f8221ce52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[7]/following::p[1]</value>
      <webElementGuid>58f38fb5-9e86-42c3-8862-fea8ccb500fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elka Verso'])[2]/following::p[1]</value>
      <webElementGuid>f71ff0a0-43f0-422d-ada1-d21bf02c8c47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[9]/preceding::p[1]</value>
      <webElementGuid>1ff2fad0-dfaa-42a2-9d38-ff868863a02f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div[3]/div/div[3]/p</value>
      <webElementGuid>6d6fa704-7b44-4336-8395-407441f67786</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.' or . = 'Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.')]</value>
      <webElementGuid>bd9e8ddd-b532-44a7-bb1c-7795f14b7aa5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
