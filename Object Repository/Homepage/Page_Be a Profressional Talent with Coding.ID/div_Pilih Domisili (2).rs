<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pilih Domisili (2)</name>
   <tag></tag>
   <elementGuidId>e0a3806e-8811-4838-ae24-46ace212cab3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.input-form</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9f7d9312-e222-49a2-8f8d-e22b4b95853b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>showOptions(); $nextTick(() => $refs.inputfocus.focus());</value>
      <webElementGuid>809f8b85-7de3-4969-a495-d16e30e2d2ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-form</value>
      <webElementGuid>396cadaf-7445-4518-acd5-1490bcbc76a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Pilih Domisili
        </value>
      <webElementGuid>5dc33e47-eb65-4473-9915-fbbeeac8ccd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[@class=&quot;Domisili dropdown block-input-item&quot;]/div[@class=&quot;input-form&quot;]</value>
      <webElementGuid>306ec9b1-5eb2-422c-84da-07fe2cd254c3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div[2]/div</value>
      <webElementGuid>7c7372d1-26ad-4513-89c7-67720958e984</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[3]/following::div[1]</value>
      <webElementGuid>e8097112-6acd-4e0b-9068-b2f2b6d5682d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/preceding::div[3]</value>
      <webElementGuid>fd8d62eb-2936-46ac-89ae-5dcb57cc6d6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pilih Domisili']/parent::*</value>
      <webElementGuid>45144d57-768b-4baf-9d0c-da85336d3c9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/div[2]/div</value>
      <webElementGuid>3ace5449-0767-4577-ae77-19ad4e9b40b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            Pilih Domisili
        ' or . = '
            Pilih Domisili
        ')]</value>
      <webElementGuid>b4e63aeb-20af-4585-a780-ef0b78e55708</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
