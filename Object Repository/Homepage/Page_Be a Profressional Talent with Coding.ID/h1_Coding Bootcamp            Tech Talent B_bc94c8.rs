<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Coding Bootcamp            Tech Talent B_bc94c8</name>
   <tag></tag>
   <elementGuidId>a24e39fc-292d-4573-89d5-e3b7a36dc568</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div/h1</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h1.new_headline_h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>02ca34a6-6fc2-4781-bc05-640fade8999d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_headline_h3</value>
      <webElementGuid>36d5c39d-6e78-4d6d-995d-5e8f9a2cfcdc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Coding Bootcamp
            Tech Talent Berkualitas</value>
      <webElementGuid>292bc9ee-f388-400d-839a-7de463b20245</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]/h1[@class=&quot;new_headline_h3&quot;]</value>
      <webElementGuid>85f5a07d-aea6-4bd4-a012-2f46e9fe2bf9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div/h1</value>
      <webElementGuid>ec9b61f6-c5cf-4851-9d20-4b986da551ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[2]/preceding::h1[1]</value>
      <webElementGuid>e6559096-3eb6-45bb-9500-fddd0aea55e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.'])[1]/preceding::h1[1]</value>
      <webElementGuid>0cf8ca26-19c8-4d19-b17c-d333fc320ef8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Coding Bootcamp']/parent::*</value>
      <webElementGuid>3ec46f4b-1235-4e74-971c-e3afad4565ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>d3121808-81be-41e3-ab9c-e706248076aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Coding Bootcamp
            Tech Talent Berkualitas' or . = 'Coding Bootcamp
            Tech Talent Berkualitas')]</value>
      <webElementGuid>3bb115a1-2d37-4f03-b0e5-79077448db2c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
