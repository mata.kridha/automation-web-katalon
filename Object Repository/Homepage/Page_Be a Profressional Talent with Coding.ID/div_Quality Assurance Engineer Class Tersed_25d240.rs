<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Quality Assurance Engineer Class Tersed_25d240</name>
   <tag></tag>
   <elementGuidId>46480b7a-1ba4-4a47-b9f3-360f24e856aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.container-kelas-choice</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>481da978-871e-46ce-ba49-6f8bb5e57c6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container-kelas-choice</value>
      <webElementGuid>a5413804-8b9c-416e-a984-1c43ef167654</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

                                
                                        
                                        Quality Assurance Engineer Class Tersedia
                                                Beasiswa
                                    
                                        
                                        Fullstack Engineer Class Tersedia
                                                Beasiswa
                                    
                                
                            </value>
      <webElementGuid>2565dee0-96f4-4b7a-a2ca-072bea45161b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-kelas-form&quot;]/div[@class=&quot;container-kelas-choice&quot;]</value>
      <webElementGuid>f076c09f-1e79-4b49-8174-1eda067ac581</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div/div[2]/div</value>
      <webElementGuid>ef0bc005-9f1f-46a2-8173-42e4d3f0d9cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[1]/following::div[1]</value>
      <webElementGuid>8038cc50-cf36-4ce6-a82b-d060232874f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div/div/div[2]/div</value>
      <webElementGuid>0882df04-a860-4020-a99a-ac38a4c4e5cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '

                                
                                        
                                        Quality Assurance Engineer Class Tersedia
                                                Beasiswa
                                    
                                        
                                        Fullstack Engineer Class Tersedia
                                                Beasiswa
                                    
                                
                            ' or . = '

                                
                                        
                                        Quality Assurance Engineer Class Tersedia
                                                Beasiswa
                                    
                                        
                                        Fullstack Engineer Class Tersedia
                                                Beasiswa
                                    
                                
                            ')]</value>
      <webElementGuid>77e90ada-839d-4d9b-9be6-cb866ad4ec1b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
