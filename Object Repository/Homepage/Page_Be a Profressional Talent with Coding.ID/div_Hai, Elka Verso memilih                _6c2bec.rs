<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Hai, Elka Verso memilih                _6c2bec</name>
   <tag></tag>
   <elementGuidId>380137ac-868e-4d4c-8bcd-a1ffdea6dbcb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3c934c1c-7b91-4d6e-bc93-8c7df8eb47af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page page-2</value>
      <webElementGuid>86539c36-7a2a-4571-b85c-9e2c2c333f19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

                        Hai, Elka Verso memilih
                            Quality Assurance Engineer Class
                        
                        Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.
                        

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
            Pilih skill
        
    

    



    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownskill', () => ({
            search: '',
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: '',
                value: ''
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == '') {
                    this.selected.label = '';
                    this.selected.value = '';
                    this.$dispatch('set-skill', '')
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.skill.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    '.skill.container-full-search')
                let containerShow = document.querySelector(
                    '.skill.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.skill.container-input-search')
                // let containerShow = document.querySelector(
                //     '.skill.container-full-search')
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';
                // containerShow.style.top = 'unset';



                // containerSearch.style.left = '24' + 'px';
                // containerSearch.style.right = '24' + 'px';
                // containerSearch.style.bottom = HEIGHT - fromtop + 'px';


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + 'px';
                // containerFullSearch.style.width = '100%';

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store('dataForm').skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch('set-skill', this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store('dataForm').skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, '');
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store('dataForm').skill = [...this.selectedArrary];
                    this.search = '';
                }
                //Alpine.store('dataForm').showOtherSkill = true;
                //Alpine.store('dataForm').readjustPosition();
                // let searchlenght = this.search.replace(/ /g, '');

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store('dataForm').activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == 'x' ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik '' diterapkan
                            

                            
                        




                        
                    </value>
      <webElementGuid>3bf042aa-70bd-451f-87cc-172676e7b880</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]</value>
      <webElementGuid>46048e91-f9ce-4356-9460-64c88c7a92b9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]</value>
      <webElementGuid>a1d4afe2-299b-44b1-a884-f829faaccbd0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indeed'])[1]/following::div[3]</value>
      <webElementGuid>ef84cde7-6d47-4455-b962-c03a3c80d669</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[8]/following::div[4]</value>
      <webElementGuid>e8883a3a-851a-4024-9cf0-6030ea4568d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div[3]/div/div[3]</value>
      <webElementGuid>1a1619db-bd16-4c96-8f39-3337a14978bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;

                        Hai, Elka Verso memilih
                            Quality Assurance Engineer Class
                        
                        Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.
                        

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
            Pilih skill
        
    

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownskill&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];
                    this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                }
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).showOtherSkill = true;
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).readjustPosition();
                // let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return &quot; , &quot;'&quot; , &quot;xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx&quot; , &quot;'&quot; , &quot;.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == &quot; , &quot;'&quot; , &quot;x&quot; , &quot;'&quot; , &quot; ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; diterapkan
                            

                            
                        




                        
                    &quot;) or . = concat(&quot;

                        Hai, Elka Verso memilih
                            Quality Assurance Engineer Class
                        
                        Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.
                        

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
            Pilih skill
        
    

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownskill&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];
                    this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                }
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).showOtherSkill = true;
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).readjustPosition();
                // let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return &quot; , &quot;'&quot; , &quot;xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx&quot; , &quot;'&quot; , &quot;.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == &quot; , &quot;'&quot; , &quot;x&quot; , &quot;'&quot; , &quot; ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; diterapkan
                            

                            
                        




                        
                    &quot;))]</value>
      <webElementGuid>18e25b00-b056-43fb-9902-992e657494ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;

                        Hai, Elka Verso memilih
                            Quality Assurance Engineer Class
                        
                        Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.
                        

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
    
            
                    Javascript 
                
        

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownskill&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];
                    this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                }
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).showOtherSkill = true;
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).readjustPosition();
                // let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return &quot; , &quot;'&quot; , &quot;xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx&quot; , &quot;'&quot; , &quot;.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == &quot; , &quot;'&quot; , &quot;x&quot; , &quot;'&quot; , &quot; ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; diterapkan
                            

                            
                        




                        
                    &quot;) or . = concat(&quot;

                        Hai, Elka Verso memilih
                            Quality Assurance Engineer Class
                        
                        Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.
                        

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
    
            
                    Javascript 
                
        

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownskill&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];
                    this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                }
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).showOtherSkill = true;
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).readjustPosition();
                // let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return &quot; , &quot;'&quot; , &quot;xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx&quot; , &quot;'&quot; , &quot;.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == &quot; , &quot;'&quot; , &quot;x&quot; , &quot;'&quot; , &quot; ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; diterapkan
                            

                            
                        




                        
                    &quot;))]</value>
      <webElementGuid>9eccf08a-ea0c-403c-b903-c0b7b7340966</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
