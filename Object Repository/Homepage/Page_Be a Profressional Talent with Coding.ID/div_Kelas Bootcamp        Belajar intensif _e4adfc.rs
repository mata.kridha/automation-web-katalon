<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Kelas Bootcamp        Belajar intensif _e4adfc</name>
   <tag></tag>
   <elementGuidId>46e69a6e-0502-4a47-8b6a-da54216e0cd7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.bootcampBlock</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>352e56f2-0fad-446b-90ca-408395b2f368</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bootcampBlock</value>
      <webElementGuid>4eb8bffe-b15c-4dc9-9c31-9342473791ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Kelas Bootcamp
        Belajar intensif bersama coach
            berpengalaman.

        
            
                                    
                        
                            
                            
                                BOOTCAMP
                                
                                    Quality Assurance Engineer Class
                                
                                
                                    
                                    
                                        Advance Level
                                    


                                
                                
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Dalam kelas ini peserta akan...
                            
                        

                        
                            Quality Assurance Engineer Class

                                                            Materi Belajar
                                

                                                                                                                        Basic Logic Programming
                                            
                                                                                                                                                                Basic Database
                                            
                                                                                                                                                                Web, Mobile and RESTful API Flow
                                            
                                                                                                                                                                Software Development and Testing Phase
                                            
                                                                                                                                                                Automatied Testing
                                            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                                                                                        Metode Pembayaran

                                
                                                                            

                                        Pay Later
                                        
                                                                        
                                
                            
                                                            
                                    Ada beasiswa
                                        100% untuk program ini

                                
                            
                                                            
                                    Daftar Sekarang
                                    
                                
                            
                            
                        
                    
                                    
                        
                            
                            
                                BOOTCAMP
                                
                                    Fullstack Engineer Class
                                
                                
                                    
                                    
                                        Advance Level
                                    


                                
                                
                                    Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Kamu sudah punya basi...
                            
                        

                        
                            Fullstack Engineer Class

                                                            Materi Belajar
                                

                                                                                                                        Basic Reactjs
                                            
                                                                                                                                                                Styling and Responsive
                                            
                                                                                                                                                                React Router Navigation
                                            
                                                                                                                                                                Using Library
                                            
                                                                                                                                                                Fetching and Auth
                                            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
                                                                                        Metode Pembayaran

                                
                                                                            

                                        Upfront Payment
                                        
                                                                        
                                
                            
                                                            
                                    Ada beasiswa
                                        100% untuk program ini

                                
                            
                                                            
                                    Daftar Sekarang
                                    
                                
                            
                            
                        
                    
                            
        


        
            Lihat Semua Kelas
        

    </value>
      <webElementGuid>b61a2629-bdc2-45db-9c39-f9c84c977236</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;bootcampBlock&quot;]</value>
      <webElementGuid>2250946d-5c87-4335-95af-794778b612df</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[2]</value>
      <webElementGuid>0269541e-cfcb-47bc-b019-18b4df4bc836</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Belajar dari Nol sampai Jago'])[1]/following::div[10]</value>
      <webElementGuid>053dfd30-081a-469b-8e75-46a443a24b4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]</value>
      <webElementGuid>4cf0f128-5ed0-43c1-9b5e-9bb616fdd221</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Kelas Bootcamp
        Belajar intensif bersama coach
            berpengalaman.

        
            
                                    
                        
                            
                            
                                BOOTCAMP
                                
                                    Quality Assurance Engineer Class
                                
                                
                                    
                                    
                                        Advance Level
                                    


                                
                                
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Dalam kelas ini peserta akan...
                            
                        

                        
                            Quality Assurance Engineer Class

                                                            Materi Belajar
                                

                                                                                                                        Basic Logic Programming
                                            
                                                                                                                                                                Basic Database
                                            
                                                                                                                                                                Web, Mobile and RESTful API Flow
                                            
                                                                                                                                                                Software Development and Testing Phase
                                            
                                                                                                                                                                Automatied Testing
                                            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                                                                                        Metode Pembayaran

                                
                                                                            

                                        Pay Later
                                        
                                                                        
                                
                            
                                                            
                                    Ada beasiswa
                                        100% untuk program ini

                                
                            
                                                            
                                    Daftar Sekarang
                                    
                                
                            
                            
                        
                    
                                    
                        
                            
                            
                                BOOTCAMP
                                
                                    Fullstack Engineer Class
                                
                                
                                    
                                    
                                        Advance Level
                                    


                                
                                
                                    Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Kamu sudah punya basi...
                            
                        

                        
                            Fullstack Engineer Class

                                                            Materi Belajar
                                

                                                                                                                        Basic Reactjs
                                            
                                                                                                                                                                Styling and Responsive
                                            
                                                                                                                                                                React Router Navigation
                                            
                                                                                                                                                                Using Library
                                            
                                                                                                                                                                Fetching and Auth
                                            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
                                                                                        Metode Pembayaran

                                
                                                                            

                                        Upfront Payment
                                        
                                                                        
                                
                            
                                                            
                                    Ada beasiswa
                                        100% untuk program ini

                                
                            
                                                            
                                    Daftar Sekarang
                                    
                                
                            
                            
                        
                    
                            
        


        
            Lihat Semua Kelas
        

    ' or . = '
        Kelas Bootcamp
        Belajar intensif bersama coach
            berpengalaman.

        
            
                                    
                        
                            
                            
                                BOOTCAMP
                                
                                    Quality Assurance Engineer Class
                                
                                
                                    
                                    
                                        Advance Level
                                    


                                
                                
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Dalam kelas ini peserta akan...
                            
                        

                        
                            Quality Assurance Engineer Class

                                                            Materi Belajar
                                

                                                                                                                        Basic Logic Programming
                                            
                                                                                                                                                                Basic Database
                                            
                                                                                                                                                                Web, Mobile and RESTful API Flow
                                            
                                                                                                                                                                Software Development and Testing Phase
                                            
                                                                                                                                                                Automatied Testing
                                            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                                                                                        Metode Pembayaran

                                
                                                                            

                                        Pay Later
                                        
                                                                        
                                
                            
                                                            
                                    Ada beasiswa
                                        100% untuk program ini

                                
                            
                                                            
                                    Daftar Sekarang
                                    
                                
                            
                            
                        
                    
                                    
                        
                            
                            
                                BOOTCAMP
                                
                                    Fullstack Engineer Class
                                
                                
                                    
                                    
                                        Advance Level
                                    


                                
                                
                                    Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Kamu sudah punya basi...
                            
                        

                        
                            Fullstack Engineer Class

                                                            Materi Belajar
                                

                                                                                                                        Basic Reactjs
                                            
                                                                                                                                                                Styling and Responsive
                                            
                                                                                                                                                                React Router Navigation
                                            
                                                                                                                                                                Using Library
                                            
                                                                                                                                                                Fetching and Auth
                                            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
                                                                                        Metode Pembayaran

                                
                                                                            

                                        Upfront Payment
                                        
                                                                        
                                
                            
                                                            
                                    Ada beasiswa
                                        100% untuk program ini

                                
                            
                                                            
                                    Daftar Sekarang
                                    
                                
                            
                            
                        
                    
                            
        


        
            Lihat Semua Kelas
        

    ')]</value>
      <webElementGuid>24c27b0c-abb4-4e9d-b0b1-79d5bec16c41</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
