<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_KAB. TEMANGGUNG</name>
   <tag></tag>
   <elementGuidId>1b006fbf-9e03-4b6d-932f-96c7855d1951</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[7]/div/div/h5</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.item-list-drop > h5.new_body1_semibold</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>63fe880b-1ebf-4974-8575-a93cef22a0d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body1_semibold</value>
      <webElementGuid>b0acbcbf-8da7-480d-a166-06b795824558</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-text</name>
      <type>Main</type>
      <value>option.label</value>
      <webElementGuid>3170c110-d01f-476a-b946-c5783defd741</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>KAB. TEMANGGUNG</value>
      <webElementGuid>defaab60-675a-48ac-8b65-9a50c9755c5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;Domisili container-full-search&quot;]/div[@class=&quot;Domisili container-show-dropdown&quot;]/div[@class=&quot;item-list-drop&quot;]/h5[@class=&quot;new_body1_semibold&quot;]</value>
      <webElementGuid>ec771602-6ec9-4c8f-8549-ea4d5a21f4c2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[7]/div/div/h5</value>
      <webElementGuid>87b6e84d-558b-4395-88f0-36cc02023206</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Belajar programming dari awal tanpa background IT'])[1]/following::h5[1]</value>
      <webElementGuid>0c633d05-7bf1-4490-b997-380b37a79e56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/following::h5[1]</value>
      <webElementGuid>8b253eb5-9f40-461d-9ec2-06e366651f1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Not Found ', '&quot;', '', '&quot;', '')])[1]/preceding::h5[1]</value>
      <webElementGuid>f1e22e58-5bcd-4405-bb8f-5ee0ec3831ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add this'])[1]/preceding::h5[1]</value>
      <webElementGuid>9cac1680-400b-448e-8936-01f8ba47ac7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='KAB. TEMANGGUNG']/parent::*</value>
      <webElementGuid>a6d01a80-7d80-405d-9679-52ac740e8890</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/h5</value>
      <webElementGuid>2fe3fec5-c380-4562-8aea-f4c1faf6f898</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'KAB. TEMANGGUNG' or . = 'KAB. TEMANGGUNG')]</value>
      <webElementGuid>1dc00052-97d4-487e-8674-717f4313e9c7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
