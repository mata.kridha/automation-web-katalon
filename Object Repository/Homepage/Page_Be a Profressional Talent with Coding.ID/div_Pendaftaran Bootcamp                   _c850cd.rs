<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pendaftaran Bootcamp                   _c850cd</name>
   <tag></tag>
   <elementGuidId>1cd515f0-19e4-4f42-9f76-dc0f2a9ec1fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='modal-register-scroll-body']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#modal-register-scroll-body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1864082c-e2fe-42e7-96a2-b51730fe9605</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>modal-register-scroll-body</value>
      <webElementGuid>0892bf97-3bc3-4d4e-9dc2-eb1562927bbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-body</value>
      <webElementGuid>447e5eaf-2578-4a16-9f96-49bad137a31d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

        
            
                Pendaftaran Bootcamp


                
                    
                

                
                    
                
            

            
                
                    1
                    
                
                
                
                    2
                    
                
                
                
                    3
                    
                
            


            
                
                    
                    
                        Pilih kelas yang ingin kamu ikuti

                        
                            
                            
                                

                                

                                


                                
                            
                        

                        
                            Pilih Kelas*

                            

                                
                                        
                                        Quality Assurance Engineer Class Tersedia
                                                Beasiswa
                                    
                                        
                                        Fullstack Engineer Class Tersedia
                                                Beasiswa
                                    
                                
                            
                        
                    

                    
                    
                        Hai, Elka Verso memilih Quality Assurance Engineer Class
                        Isi data berikut yuk, supaya
                            kamu dapat penawaran yang cocok untukmu
                        

                            
                                Nomor Whatsapp*
                                
                                
                                
                            


                            
    Domisili Saat Ini*
    
    
    
    KAB TIMOR TENGAH SELATAN
    


    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownDomisili', () => ({
            search: '',
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: '',
                value: ''
            },
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.domisili)
                    if (this.$store.dataForm.domisili == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }

                    if (this.$store.dataForm.domisiliFocus) {
                        this.showOptions();
                        this.$store.dataForm.domisiliFocus = false;


                        setTimeout(() => {
                            let searchFetch = document.getElementById(
                                'searchFetch');
                            searchFetch.focus();
                        }, 2000);

                        // console.log(&quot;show options&quot;)
                    } else {

                        // console.log(&quot;hide options&quot;)
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == '') {
                    this.selected.label = '';
                    this.selected.value = '';
                    this.$dispatch('set-domisili', '')
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.Domisili.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    '.Domisili.container-full-search')
                let containerShow = document.querySelector(
                    '.Domisili.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.Domisili.container-input-search')
                // let containerShow = document.querySelector(
                //     '.Domisili.container-full-search')
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';
                // containerShow.style.top = 'unset';



                // containerSearch.style.left = '24' + 'px';
                // containerSearch.style.right = '24' + 'px';
                // containerSearch.style.bottom = HEIGHT - fromtop + 'px';


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + 'px';
                // containerFullSearch.style.width = '100%';

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch('set-domisili', this.selected.value)
            },
            // addOther() {
            //     let option = {
            //         label: this.search,
            //         value: this.search
            //     }

            //     this.selected = option
            // },

        }))
    })

                            
                            

                            
                                Pendidikan
                                    Terakhir*
                                

                                
                                    
                                        
                                        Universitas/
                                            Sederajat
                                    

                                    
                                        
                                        Diploma
                                    

                                    
                                        
                                        SMA/SMK
                                    

                                    
                                        
                                        Yang
                                            lain
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Jurusan*
                                
                                
                            



                            
                                
    Nama Instansi Pendidikan?*
    
    
    
    Akademi Akuntansi Bandung

    



    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownInstansi', () => ({
            search: '',
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: '',
                value: ''
            },
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.instansi)
                    if (this.$store.dataForm.instansi == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == '') {
                    this.selected.label = '';
                    this.selected.value = '';
                    this.$dispatch('set-instansi', '')
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.Instansi.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    '.Instansi.container-full-search')
                let containerShow = document.querySelector(
                    '.Instansi.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.Instansi.container-input-search')
                // let containerShow = document.querySelector(
                //     '.Instansi.container-full-search')
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';
                // containerShow.style.top = 'unset';



                // containerSearch.style.left = '24' + 'px';
                // containerSearch.style.right = '24' + 'px';
                // containerSearch.style.bottom = HEIGHT - fromtop + 'px';


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + 'px';
                // containerFullSearch.style.width = '100%';

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch('set-instansi', this.selected.value)
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, '');

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },

        }))
    })

                                

                            

                            
                                Nama Instansi
                                    Pendidikan?*
                                
                                
                            

                            
                            
                            
                            

                            
                            
    Darimana kamu mengetahui Coding.ID?*
    
    

    
    Indeed
    
    

    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownReference', () => ({
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.reference)
                    if (this.$store.dataForm.reference == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }

                })

                // Alpine.effect(() => {
                //     if (this.$store.dataForm.domisilifocus) {
                //         this.showOptions();
                //         console.log(&quot;show options&quot;)
                //     } else {

                //         console.log(&quot;hide options&quot;)
                //     }
                // })

            },
            search: '',
            optionsVisible: false,
            selected: {
                label: '',
                value: ''
            },
            hideOptions() {
                this.optionsVisible = false

            },
            showOptions() {
                let HEIGHT = this.$refs.parentform.offsetHeight;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.Reference.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)

                let bottom = HEIGHT - fromtop + 57;;

                let containerFullSearch = document.querySelector(
                    '.Reference.container-full-search')
                let containerShow = document.querySelector(
                    '.Reference.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.Reference.container-input-search')

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // containerShow.style.bottom = bottom + 'px';
                // containerShow.style.top = 'unset';

                // containerShow.style.top = top + 'px';
                // console.log(left)
                // console.log(top)
                //console.log(bottom + &quot;bottom&quot;)
            },
            selectOption(option) {
                this.selected = option
                this.hideOptions()
                this.search = option.label
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, '');

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },
            options: [{&quot;label&quot;:&quot;POPFM&quot;,&quot;value&quot;:14},{&quot;label&quot;:&quot;Email Blast Coding.ID&quot;,&quot;value&quot;:13},{&quot;label&quot;:&quot;Coding.ID Mini Class\/ Webinar&quot;,&quot;value&quot;:12},{&quot;label&quot;:&quot;Kerabat\/ Teman&quot;,&quot;value&quot;:11},{&quot;label&quot;:&quot;Campus&quot;,&quot;value&quot;:10},{&quot;label&quot;:&quot;Indeed&quot;,&quot;value&quot;:9},{&quot;label&quot;:&quot;Jobstreet&quot;,&quot;value&quot;:8},{&quot;label&quot;:&quot;WhatsApp&quot;,&quot;value&quot;:7},{&quot;label&quot;:&quot;Youtube&quot;,&quot;value&quot;:6},{&quot;label&quot;:&quot;Discord&quot;,&quot;value&quot;:5},{&quot;label&quot;:&quot;Telegram&quot;,&quot;value&quot;:4},{&quot;label&quot;:&quot;Linked.In&quot;,&quot;value&quot;:3},{&quot;label&quot;:&quot;Facebook&quot;,&quot;value&quot;:2},{&quot;label&quot;:&quot;Instagram&quot;,&quot;value&quot;:1}],
            filterOptions() {

                return this.options.filter(option => {
                    return option.label.toLowerCase().includes(this.search.replace(/ /g, '')
                        .toLowerCase())
                })
            },
        }))
    })

                            
                            
                            
                        
                    

                    
                    

                        Hai, Elka Verso memilih
                            Quality Assurance Engineer Class
                        
                        Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.
                        

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
            Pilih skill
        
    

    



    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownskill', () => ({
            search: '',
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: '',
                value: ''
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == '') {
                    this.selected.label = '';
                    this.selected.value = '';
                    this.$dispatch('set-skill', '')
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.skill.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    '.skill.container-full-search')
                let containerShow = document.querySelector(
                    '.skill.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.skill.container-input-search')
                // let containerShow = document.querySelector(
                //     '.skill.container-full-search')
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';
                // containerShow.style.top = 'unset';



                // containerSearch.style.left = '24' + 'px';
                // containerSearch.style.right = '24' + 'px';
                // containerSearch.style.bottom = HEIGHT - fromtop + 'px';


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + 'px';
                // containerFullSearch.style.width = '100%';

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store('dataForm').skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch('set-skill', this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store('dataForm').skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, '');
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store('dataForm').skill = [...this.selectedArrary];
                    this.search = '';
                }
                //Alpine.store('dataForm').showOtherSkill = true;
                //Alpine.store('dataForm').readjustPosition();
                // let searchlenght = this.search.replace(/ /g, '');

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store('dataForm').activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == 'x' ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik '' diterapkan
                            

                            
                        




                        
                    


                
            


            
                
                    
                        Sebelumnya
                    

                    Daftar Bootcamp
                

                Punya
                    pertanyaan
                    seputar bootcamp yang cocok
                    untukmu?
                    Konsultasikan dengan kami lewat Whatsapp
            


            
                
                    Perhatian
                    Pesan
                    
                        Ok
                    

                    

                    
                
            
        Belajar programming dari awal tanpa background IT
            
                
                            KAB TIMOR TENGAH SELATAN
                        
                            KAB. ACEH BARAT
                        
                            KAB. ACEH BARAT DAYA
                        
                            KAB. ACEH BESAR
                        
                            KAB. ACEH JAYA
                        
                            KAB. ACEH SELATAN
                        
                            KAB. ACEH SINGKIL
                        
                            KAB. ACEH TAMIANG
                        
                            KAB. ACEH TENGAH
                        
                            KAB. ACEH TENGGARA
                        
                            KAB. ACEH TIMUR
                        
                            KAB. ACEH UTARA
                        
                            KAB. ADM. KEP. SERIBU
                        
                            KAB. AGAM
                        
                            KAB. ALOR
                        
                
                
                        Load more
                    

                

            
            
                
            
        
            
                
                            AKADEMI ADMINISTRASI RUMAH SAKIT MATARAM
                        
                            Akademi Akuntansi (AKTAN) Boekittinggi
                        
                            Akademi Akuntansi Bandung
                        
                            Akademi Akuntansi Bina Insani
                        
                            Akademi Akuntansi Borobudur
                        
                            Akademi Akuntansi Dan Komputer Stephen Jambi
                        
                            Akademi Akuntansi Dan Manajemen Mitra Lampung
                        
                            Akademi Akuntansi Dan Manajemen Pembangunan
                        
                            Akademi Akuntansi Denpasar
                        
                            Akademi Akuntansi Effendi Harahap
                        
                            Akademi Akuntansi Indonesia Padang
                        
                            Akademi Akuntansi Jayabaya
                        
                            Akademi Akuntansi Keuangan Dan Perbankan Indonesia
                        
                            Akademi Akuntansi Lampung
                        
                            Akademi Akuntansi Muhammadiyah Klaten
                        
                
                
                        Load more
                    

                

            
            
                
            
        
            
                
                        Indeed
                    

                
            
            
                
            
        
            
                
                            C#
                        
                            C++
                        
                            Java
                        
                            Javascript
                        
                            PHP
                        
                            Python
                        
                            Visual Basic .NET
                        
                
                

                

            
            
                
            
        
    </value>
      <webElementGuid>77d5c627-a8ad-43f4-b78a-6abab16c5d00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modal-register-scroll-body&quot;)</value>
      <webElementGuid>1259b0ef-53ef-416e-9d2c-26fea3855456</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='modal-register-scroll-body']</value>
      <webElementGuid>5e54acf9-07b2-4645-beca-3d8b30bba1c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modal-registration']/div</value>
      <webElementGuid>62364711-3c25-40e2-a836-42f7b0386ec6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Semua Events'])[1]/following::div[2]</value>
      <webElementGuid>1b8ca884-469d-4a09-977b-46620600e6af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Semua Course'])[2]/following::div[2]</value>
      <webElementGuid>9768f609-6665-49c5-92c1-507d8aa6a967</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[8]/div</value>
      <webElementGuid>d69930bf-761c-4e62-9fae-19199034d056</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'modal-register-scroll-body' and (text() = concat(&quot;

        
            
                Pendaftaran Bootcamp


                
                    
                

                
                    
                
            

            
                
                    1
                    
                
                
                
                    2
                    
                
                
                
                    3
                    
                
            


            
                
                    
                    
                        Pilih kelas yang ingin kamu ikuti

                        
                            
                            
                                

                                

                                


                                
                            
                        

                        
                            Pilih Kelas*

                            

                                
                                        
                                        Quality Assurance Engineer Class Tersedia
                                                Beasiswa
                                    
                                        
                                        Fullstack Engineer Class Tersedia
                                                Beasiswa
                                    
                                
                            
                        
                    

                    
                    
                        Hai, Elka Verso memilih Quality Assurance Engineer Class
                        Isi data berikut yuk, supaya
                            kamu dapat penawaran yang cocok untukmu
                        

                            
                                Nomor Whatsapp*
                                
                                
                                
                            


                            
    Domisili Saat Ini*
    
    
    
    KAB TIMOR TENGAH SELATAN
    


    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownDomisili&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.domisili)
                    if (this.$store.dataForm.domisili == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                    if (this.$store.dataForm.domisiliFocus) {
                        this.showOptions();
                        this.$store.dataForm.domisiliFocus = false;


                        setTimeout(() => {
                            let searchFetch = document.getElementById(
                                &quot; , &quot;'&quot; , &quot;searchFetch&quot; , &quot;'&quot; , &quot;);
                            searchFetch.focus();
                        }, 2000);

                        // console.log(&quot;show options&quot;)
                    } else {

                        // console.log(&quot;hide options&quot;)
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            // addOther() {
            //     let option = {
            //         label: this.search,
            //         value: this.search
            //     }

            //     this.selected = option
            // },

        }))
    })

                            
                            

                            
                                Pendidikan
                                    Terakhir*
                                

                                
                                    
                                        
                                        Universitas/
                                            Sederajat
                                    

                                    
                                        
                                        Diploma
                                    

                                    
                                        
                                        SMA/SMK
                                    

                                    
                                        
                                        Yang
                                            lain
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Jurusan*
                                
                                
                            



                            
                                
    Nama Instansi Pendidikan?*
    
    
    
    Akademi Akuntansi Bandung

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownInstansi&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.instansi)
                    if (this.$store.dataForm.instansi == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },

        }))
    })

                                

                            

                            
                                Nama Instansi
                                    Pendidikan?*
                                
                                
                            

                            
                            
                            
                            

                            
                            
    Darimana kamu mengetahui Coding.ID?*
    
    

    
    Indeed
    
    

    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownReference&quot; , &quot;'&quot; , &quot;, () => ({
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.reference)
                    if (this.$store.dataForm.reference == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                })

                // Alpine.effect(() => {
                //     if (this.$store.dataForm.domisilifocus) {
                //         this.showOptions();
                //         console.log(&quot;show options&quot;)
                //     } else {

                //         console.log(&quot;hide options&quot;)
                //     }
                // })

            },
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            optionsVisible: false,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            hideOptions() {
                this.optionsVisible = false

            },
            showOptions() {
                let HEIGHT = this.$refs.parentform.offsetHeight;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)

                let bottom = HEIGHT - fromtop + 57;;

                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-input-search&quot; , &quot;'&quot; , &quot;)

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.bottom = bottom + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.top = top + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // console.log(left)
                // console.log(top)
                //console.log(bottom + &quot;bottom&quot;)
            },
            selectOption(option) {
                this.selected = option
                this.hideOptions()
                this.search = option.label
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },
            options: [{&quot;label&quot;:&quot;POPFM&quot;,&quot;value&quot;:14},{&quot;label&quot;:&quot;Email Blast Coding.ID&quot;,&quot;value&quot;:13},{&quot;label&quot;:&quot;Coding.ID Mini Class\/ Webinar&quot;,&quot;value&quot;:12},{&quot;label&quot;:&quot;Kerabat\/ Teman&quot;,&quot;value&quot;:11},{&quot;label&quot;:&quot;Campus&quot;,&quot;value&quot;:10},{&quot;label&quot;:&quot;Indeed&quot;,&quot;value&quot;:9},{&quot;label&quot;:&quot;Jobstreet&quot;,&quot;value&quot;:8},{&quot;label&quot;:&quot;WhatsApp&quot;,&quot;value&quot;:7},{&quot;label&quot;:&quot;Youtube&quot;,&quot;value&quot;:6},{&quot;label&quot;:&quot;Discord&quot;,&quot;value&quot;:5},{&quot;label&quot;:&quot;Telegram&quot;,&quot;value&quot;:4},{&quot;label&quot;:&quot;Linked.In&quot;,&quot;value&quot;:3},{&quot;label&quot;:&quot;Facebook&quot;,&quot;value&quot;:2},{&quot;label&quot;:&quot;Instagram&quot;,&quot;value&quot;:1}],
            filterOptions() {

                return this.options.filter(option => {
                    return option.label.toLowerCase().includes(this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                        .toLowerCase())
                })
            },
        }))
    })

                            
                            
                            
                        
                    

                    
                    

                        Hai, Elka Verso memilih
                            Quality Assurance Engineer Class
                        
                        Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.
                        

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
            Pilih skill
        
    

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownskill&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];
                    this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                }
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).showOtherSkill = true;
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).readjustPosition();
                // let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return &quot; , &quot;'&quot; , &quot;xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx&quot; , &quot;'&quot; , &quot;.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == &quot; , &quot;'&quot; , &quot;x&quot; , &quot;'&quot; , &quot; ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; diterapkan
                            

                            
                        




                        
                    


                
            


            
                
                    
                        Sebelumnya
                    

                    Daftar Bootcamp
                

                Punya
                    pertanyaan
                    seputar bootcamp yang cocok
                    untukmu?
                    Konsultasikan dengan kami lewat Whatsapp
            


            
                
                    Perhatian
                    Pesan
                    
                        Ok
                    

                    

                    
                
            
        Belajar programming dari awal tanpa background IT
            
                
                            KAB TIMOR TENGAH SELATAN
                        
                            KAB. ACEH BARAT
                        
                            KAB. ACEH BARAT DAYA
                        
                            KAB. ACEH BESAR
                        
                            KAB. ACEH JAYA
                        
                            KAB. ACEH SELATAN
                        
                            KAB. ACEH SINGKIL
                        
                            KAB. ACEH TAMIANG
                        
                            KAB. ACEH TENGAH
                        
                            KAB. ACEH TENGGARA
                        
                            KAB. ACEH TIMUR
                        
                            KAB. ACEH UTARA
                        
                            KAB. ADM. KEP. SERIBU
                        
                            KAB. AGAM
                        
                            KAB. ALOR
                        
                
                
                        Load more
                    

                

            
            
                
            
        
            
                
                            AKADEMI ADMINISTRASI RUMAH SAKIT MATARAM
                        
                            Akademi Akuntansi (AKTAN) Boekittinggi
                        
                            Akademi Akuntansi Bandung
                        
                            Akademi Akuntansi Bina Insani
                        
                            Akademi Akuntansi Borobudur
                        
                            Akademi Akuntansi Dan Komputer Stephen Jambi
                        
                            Akademi Akuntansi Dan Manajemen Mitra Lampung
                        
                            Akademi Akuntansi Dan Manajemen Pembangunan
                        
                            Akademi Akuntansi Denpasar
                        
                            Akademi Akuntansi Effendi Harahap
                        
                            Akademi Akuntansi Indonesia Padang
                        
                            Akademi Akuntansi Jayabaya
                        
                            Akademi Akuntansi Keuangan Dan Perbankan Indonesia
                        
                            Akademi Akuntansi Lampung
                        
                            Akademi Akuntansi Muhammadiyah Klaten
                        
                
                
                        Load more
                    

                

            
            
                
            
        
            
                
                        Indeed
                    

                
            
            
                
            
        
            
                
                            C#
                        
                            C++
                        
                            Java
                        
                            Javascript
                        
                            PHP
                        
                            Python
                        
                            Visual Basic .NET
                        
                
                

                

            
            
                
            
        
    &quot;) or . = concat(&quot;

        
            
                Pendaftaran Bootcamp


                
                    
                

                
                    
                
            

            
                
                    1
                    
                
                
                
                    2
                    
                
                
                
                    3
                    
                
            


            
                
                    
                    
                        Pilih kelas yang ingin kamu ikuti

                        
                            
                            
                                

                                

                                


                                
                            
                        

                        
                            Pilih Kelas*

                            

                                
                                        
                                        Quality Assurance Engineer Class Tersedia
                                                Beasiswa
                                    
                                        
                                        Fullstack Engineer Class Tersedia
                                                Beasiswa
                                    
                                
                            
                        
                    

                    
                    
                        Hai, Elka Verso memilih Quality Assurance Engineer Class
                        Isi data berikut yuk, supaya
                            kamu dapat penawaran yang cocok untukmu
                        

                            
                                Nomor Whatsapp*
                                
                                
                                
                            


                            
    Domisili Saat Ini*
    
    
    
    KAB TIMOR TENGAH SELATAN
    


    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownDomisili&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.domisili)
                    if (this.$store.dataForm.domisili == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                    if (this.$store.dataForm.domisiliFocus) {
                        this.showOptions();
                        this.$store.dataForm.domisiliFocus = false;


                        setTimeout(() => {
                            let searchFetch = document.getElementById(
                                &quot; , &quot;'&quot; , &quot;searchFetch&quot; , &quot;'&quot; , &quot;);
                            searchFetch.focus();
                        }, 2000);

                        // console.log(&quot;show options&quot;)
                    } else {

                        // console.log(&quot;hide options&quot;)
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            // addOther() {
            //     let option = {
            //         label: this.search,
            //         value: this.search
            //     }

            //     this.selected = option
            // },

        }))
    })

                            
                            

                            
                                Pendidikan
                                    Terakhir*
                                

                                
                                    
                                        
                                        Universitas/
                                            Sederajat
                                    

                                    
                                        
                                        Diploma
                                    

                                    
                                        
                                        SMA/SMK
                                    

                                    
                                        
                                        Yang
                                            lain
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Jurusan*
                                
                                
                            



                            
                                
    Nama Instansi Pendidikan?*
    
    
    
    Akademi Akuntansi Bandung

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownInstansi&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.instansi)
                    if (this.$store.dataForm.instansi == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },

        }))
    })

                                

                            

                            
                                Nama Instansi
                                    Pendidikan?*
                                
                                
                            

                            
                            
                            
                            

                            
                            
    Darimana kamu mengetahui Coding.ID?*
    
    

    
    Indeed
    
    

    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownReference&quot; , &quot;'&quot; , &quot;, () => ({
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.reference)
                    if (this.$store.dataForm.reference == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                })

                // Alpine.effect(() => {
                //     if (this.$store.dataForm.domisilifocus) {
                //         this.showOptions();
                //         console.log(&quot;show options&quot;)
                //     } else {

                //         console.log(&quot;hide options&quot;)
                //     }
                // })

            },
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            optionsVisible: false,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            hideOptions() {
                this.optionsVisible = false

            },
            showOptions() {
                let HEIGHT = this.$refs.parentform.offsetHeight;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)

                let bottom = HEIGHT - fromtop + 57;;

                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-input-search&quot; , &quot;'&quot; , &quot;)

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.bottom = bottom + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.top = top + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // console.log(left)
                // console.log(top)
                //console.log(bottom + &quot;bottom&quot;)
            },
            selectOption(option) {
                this.selected = option
                this.hideOptions()
                this.search = option.label
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },
            options: [{&quot;label&quot;:&quot;POPFM&quot;,&quot;value&quot;:14},{&quot;label&quot;:&quot;Email Blast Coding.ID&quot;,&quot;value&quot;:13},{&quot;label&quot;:&quot;Coding.ID Mini Class\/ Webinar&quot;,&quot;value&quot;:12},{&quot;label&quot;:&quot;Kerabat\/ Teman&quot;,&quot;value&quot;:11},{&quot;label&quot;:&quot;Campus&quot;,&quot;value&quot;:10},{&quot;label&quot;:&quot;Indeed&quot;,&quot;value&quot;:9},{&quot;label&quot;:&quot;Jobstreet&quot;,&quot;value&quot;:8},{&quot;label&quot;:&quot;WhatsApp&quot;,&quot;value&quot;:7},{&quot;label&quot;:&quot;Youtube&quot;,&quot;value&quot;:6},{&quot;label&quot;:&quot;Discord&quot;,&quot;value&quot;:5},{&quot;label&quot;:&quot;Telegram&quot;,&quot;value&quot;:4},{&quot;label&quot;:&quot;Linked.In&quot;,&quot;value&quot;:3},{&quot;label&quot;:&quot;Facebook&quot;,&quot;value&quot;:2},{&quot;label&quot;:&quot;Instagram&quot;,&quot;value&quot;:1}],
            filterOptions() {

                return this.options.filter(option => {
                    return option.label.toLowerCase().includes(this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                        .toLowerCase())
                })
            },
        }))
    })

                            
                            
                            
                        
                    

                    
                    

                        Hai, Elka Verso memilih
                            Quality Assurance Engineer Class
                        
                        Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.
                        

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
            Pilih skill
        
    

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownskill&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];
                    this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                }
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).showOtherSkill = true;
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).readjustPosition();
                // let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return &quot; , &quot;'&quot; , &quot;xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx&quot; , &quot;'&quot; , &quot;.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == &quot; , &quot;'&quot; , &quot;x&quot; , &quot;'&quot; , &quot; ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; diterapkan
                            

                            
                        




                        
                    


                
            


            
                
                    
                        Sebelumnya
                    

                    Daftar Bootcamp
                

                Punya
                    pertanyaan
                    seputar bootcamp yang cocok
                    untukmu?
                    Konsultasikan dengan kami lewat Whatsapp
            


            
                
                    Perhatian
                    Pesan
                    
                        Ok
                    

                    

                    
                
            
        Belajar programming dari awal tanpa background IT
            
                
                            KAB TIMOR TENGAH SELATAN
                        
                            KAB. ACEH BARAT
                        
                            KAB. ACEH BARAT DAYA
                        
                            KAB. ACEH BESAR
                        
                            KAB. ACEH JAYA
                        
                            KAB. ACEH SELATAN
                        
                            KAB. ACEH SINGKIL
                        
                            KAB. ACEH TAMIANG
                        
                            KAB. ACEH TENGAH
                        
                            KAB. ACEH TENGGARA
                        
                            KAB. ACEH TIMUR
                        
                            KAB. ACEH UTARA
                        
                            KAB. ADM. KEP. SERIBU
                        
                            KAB. AGAM
                        
                            KAB. ALOR
                        
                
                
                        Load more
                    

                

            
            
                
            
        
            
                
                            AKADEMI ADMINISTRASI RUMAH SAKIT MATARAM
                        
                            Akademi Akuntansi (AKTAN) Boekittinggi
                        
                            Akademi Akuntansi Bandung
                        
                            Akademi Akuntansi Bina Insani
                        
                            Akademi Akuntansi Borobudur
                        
                            Akademi Akuntansi Dan Komputer Stephen Jambi
                        
                            Akademi Akuntansi Dan Manajemen Mitra Lampung
                        
                            Akademi Akuntansi Dan Manajemen Pembangunan
                        
                            Akademi Akuntansi Denpasar
                        
                            Akademi Akuntansi Effendi Harahap
                        
                            Akademi Akuntansi Indonesia Padang
                        
                            Akademi Akuntansi Jayabaya
                        
                            Akademi Akuntansi Keuangan Dan Perbankan Indonesia
                        
                            Akademi Akuntansi Lampung
                        
                            Akademi Akuntansi Muhammadiyah Klaten
                        
                
                
                        Load more
                    

                

            
            
                
            
        
            
                
                        Indeed
                    

                
            
            
                
            
        
            
                
                            C#
                        
                            C++
                        
                            Java
                        
                            Javascript
                        
                            PHP
                        
                            Python
                        
                            Visual Basic .NET
                        
                
                

                

            
            
                
            
        
    &quot;))]</value>
      <webElementGuid>79409202-bb58-47af-b00c-f3266dae9625</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
