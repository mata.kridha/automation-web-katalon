<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Javascript_1</name>
   <tag></tag>
   <elementGuidId>6cfc3ee1-e676-407f-85eb-b94ad4c60080</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div[2]/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.input-form.container-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>cda3e8af-ecbf-4fd1-8028-3314dc693ab7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>showOptions();</value>
      <webElementGuid>fe9b4ea4-153c-45d7-9151-bc9e9cff1eec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-form container-item</value>
      <webElementGuid>c4541689-2f0f-4f68-901e-3110f74993de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                    Javascript 
                
        </value>
      <webElementGuid>af7027e3-b7e9-4855-8a92-0e9c5c277d4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[2]/div[@class=&quot;skill dropdown block-input-item&quot;]/div[@class=&quot;input-form container-item&quot;]</value>
      <webElementGuid>993db70c-0f7a-4032-a02e-343654d6fd84</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div[2]/div/div</value>
      <webElementGuid>e4c1b1af-1c26-415c-85b6-cf6dd9c02f8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[10]/following::div[1]</value>
      <webElementGuid>e06a5c32-cfa9-4e21-a655-b9994f3cf2fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[11]/preceding::div[4]</value>
      <webElementGuid>25d4e5b6-ef69-4c19-8e4a-b29a8c7b8265</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]/div/div[2]/div/div</value>
      <webElementGuid>5a83c033-65d8-4339-a97c-d76fc751e83a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
                    Javascript 
                
        ' or . = '
            
                    Javascript 
                
        ')]</value>
      <webElementGuid>e4f53e77-e89c-4e53-b166-888e52530c31</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
