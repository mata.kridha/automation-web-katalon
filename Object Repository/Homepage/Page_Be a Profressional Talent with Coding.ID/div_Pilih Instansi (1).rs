<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pilih Instansi (1)</name>
   <tag></tag>
   <elementGuidId>6a46034d-5ac7-42b8-bbbc-039dc4c23dba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div[6]/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.Instansi.dropdown.block-input-item > div.input-form</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>374fd51c-10b0-49d3-87ab-65616c82adce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>showOptions(); $nextTick(() => $refs.inputfocus.focus());</value>
      <webElementGuid>775b5d50-600a-4075-953a-495ed07e968f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-form</value>
      <webElementGuid>d5a464ca-16f7-4efe-a799-0fe17352e415</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Pilih Instansi
        </value>
      <webElementGuid>e80a8a18-a5c3-41ee-ba5f-f341558d3bce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[6]/div[@class=&quot;Instansi dropdown block-input-item&quot;]/div[@class=&quot;input-form&quot;]</value>
      <webElementGuid>5ee0962d-d665-4697-91c5-2a1368ba5fe9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div[6]/div/div</value>
      <webElementGuid>a7a2590f-6e7a-4749-aec1-36f56563e6f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[6]/following::div[1]</value>
      <webElementGuid>0fb813a5-1001-4148-944b-c135e1aef1ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[7]/preceding::div[3]</value>
      <webElementGuid>205aef33-74f4-46f5-ab26-c6d8efc61866</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pilih Instansi']/parent::*</value>
      <webElementGuid>63af5dc4-10f8-47c5-a0a8-71b3cb95f672</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div</value>
      <webElementGuid>27f06ce8-6e76-4b27-a372-38894b5932ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            Pilih Instansi
        ' or . = '
            Pilih Instansi
        ')]</value>
      <webElementGuid>9fd976a1-7a78-4a27-aa42-3b1eb2db36e3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
