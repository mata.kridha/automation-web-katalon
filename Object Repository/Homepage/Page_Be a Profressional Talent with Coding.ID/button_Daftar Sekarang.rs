<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Daftar Sekarang</name>
   <tag></tag>
   <elementGuidId>05233405-857c-42f7-9433-c74c012739fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@onclick=&quot;window.location.href='https://demo-app.online/register_bootcamp/home/4';&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6e0e9f12-f774-4220-9803-016bc806043a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bootcamp</name>
      <type>Main</type>
      <value>Quality Assurance Engineer Class</value>
      <webElementGuid>28ccca10-3f97-4871-9e9b-992b809c08b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>window.location.href='https://demo-app.online/register_bootcamp/home/4';</value>
      <webElementGuid>6c301874-cb5c-4f1c-95be-9ef26af60345</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>daftarBootcamp </value>
      <webElementGuid>3517ff13-0efb-40d2-b6fb-0788fd366849</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    Daftar Sekarang
                                    
                                </value>
      <webElementGuid>b3d32638-6f3b-42da-b366-db1140b18eac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;bootcampBlock&quot;]/div[@class=&quot;blockBootcamp&quot;]/ul[@class=&quot;listBootcamp&quot;]/li[@class=&quot;listBootcampItem&quot;]/div[@class=&quot;tooltip-right&quot;]/button[@class=&quot;daftarBootcamp&quot;]</value>
      <webElementGuid>66edf916-3d8c-401c-b589-0740a877beb0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@onclick=&quot;window.location.href='https://demo-app.online/register_bootcamp/home/4';&quot;]</value>
      <webElementGuid>15221a15-c8c2-4d37-9391-4023859e0c95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div[2]/button</value>
      <webElementGuid>8b781763-b3f0-4ae1-9ad9-fdd388f96fff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pay Later'])[1]/following::button[1]</value>
      <webElementGuid>06c4c66e-d1f2-4161-9d57-a66fc7531a08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BOOTCAMP'])[2]/preceding::button[1]</value>
      <webElementGuid>755101af-9b3a-450c-80ff-995db7c0c313</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fullstack Engineer Class'])[3]/preceding::button[1]</value>
      <webElementGuid>d4cbe4a2-cd54-4842-a802-dae094a3ab38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div[2]/button</value>
      <webElementGuid>6adc7a58-461a-4938-8a7c-2efb24565deb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                    Daftar Sekarang
                                    
                                ' or . = '
                                    Daftar Sekarang
                                    
                                ')]</value>
      <webElementGuid>d365dc4f-ba43-48b8-bc01-075fa2ba4289</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
