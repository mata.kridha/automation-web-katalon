<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pilih Referensi (2)</name>
   <tag></tag>
   <elementGuidId>cbfd605d-23f4-41e4-b9b9-6a28f356d951</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.Reference.dropdown.block-input-item > div.input-form</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div[8]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c8e44456-27e3-4aec-b064-6444b1da78f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>showOptions(); $nextTick(() => $refs.inputfocus.focus());</value>
      <webElementGuid>66354959-2079-4cb3-be70-bf42395b13b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-form</value>
      <webElementGuid>cd94f30d-4423-4526-9856-0711b4e27359</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Pilih Referensi
        </value>
      <webElementGuid>bbfb3613-a93d-4642-b799-08278f343971</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[@class=&quot;Reference dropdown block-input-item&quot;]/div[@class=&quot;input-form&quot;]</value>
      <webElementGuid>29d53514-8a8f-4d3c-92c0-b4e5103b4ac8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div[8]/div</value>
      <webElementGuid>350d3f6f-c983-4710-8015-ac3c19c78e16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[8]/following::div[1]</value>
      <webElementGuid>08fc52ca-cbf4-4a6b-8d24-85051498562a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elka Verso'])[2]/preceding::div[3]</value>
      <webElementGuid>65c2573d-48c7-4a21-97d5-0addb5dcaa61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pilih Referensi']/parent::*</value>
      <webElementGuid>be7682ac-2d15-44a7-9d99-f6b3723076b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[8]/div</value>
      <webElementGuid>40444009-90aa-4c3a-8913-c8e478c5e79a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            Pilih Referensi
        ' or . = '
            Pilih Referensi
        ')]</value>
      <webElementGuid>38645107-7e05-4dda-b84e-12ef7f2df8d3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
