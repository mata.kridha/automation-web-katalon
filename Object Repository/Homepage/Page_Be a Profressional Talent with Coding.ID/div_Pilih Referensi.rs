<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pilih Referensi</name>
   <tag></tag>
   <elementGuidId>0374eb33-c78a-458f-a57d-11d769245527</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.Reference.dropdown.block-input-item > div.input-form</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div[8]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>36d16ebd-eaef-44cf-b630-7dddb1ce2538</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>showOptions(); $nextTick(() => $refs.inputfocus.focus());</value>
      <webElementGuid>e1213c71-280d-4bf7-9704-6f7c8cefc729</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-form</value>
      <webElementGuid>ea3c4a8e-debd-43dd-ac7e-a513d43bd3eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Pilih Referensi
        </value>
      <webElementGuid>30a9aeb0-b1a8-4aa4-9146-14dcb644c71d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[@class=&quot;Reference dropdown block-input-item&quot;]/div[@class=&quot;input-form&quot;]</value>
      <webElementGuid>44ebaa13-2bdd-4198-9751-ec3e488a7c2f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div[8]/div</value>
      <webElementGuid>64f69194-b3d3-4697-a41a-eb23acfc9d4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[8]/following::div[1]</value>
      <webElementGuid>f3fdb4c5-acff-4219-9c4d-007e9ad25b8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elka Verso'])[2]/preceding::div[3]</value>
      <webElementGuid>97bc88c8-d8f9-4537-84fa-eab0dbe539b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pilih Referensi']/parent::*</value>
      <webElementGuid>87a8145f-a0e0-4917-9c62-004ab5fd32cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[8]/div</value>
      <webElementGuid>fa35fb8f-bdf4-4bd0-8a08-e247c331c7ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            Pilih Referensi
        ' or . = '
            Pilih Referensi
        ')]</value>
      <webElementGuid>d36619f9-a83c-4001-b8f0-1c9db04c02cd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
