<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_C</name>
   <tag></tag>
   <elementGuidId>5289ce5e-0e6b-42be-adb5-7f10779a09a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[10]/div/div/h5</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.skill.container-show-dropdown > div.item-list-drop > h5.new_body1_semibold</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>1da5e3de-a169-4935-8620-b8058971bd66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body1_semibold</value>
      <webElementGuid>53285297-b3a0-4979-8b9d-34dbf6d7cae6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-text</name>
      <type>Main</type>
      <value>option.label</value>
      <webElementGuid>4a5da628-298c-4e6e-a41e-7c04e82e951c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>C#</value>
      <webElementGuid>ae2f95ef-174b-48e3-8b4b-dfeeaa8e70f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;skill container-full-search&quot;]/div[@class=&quot;skill container-show-dropdown&quot;]/div[@class=&quot;item-list-drop&quot;]/h5[@class=&quot;new_body1_semibold&quot;]</value>
      <webElementGuid>60692214-397d-45a8-b2ce-3d998ba6a300</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[10]/div/div/h5</value>
      <webElementGuid>d3ecaf7d-acc0-4bcb-b58f-1220d4ebe9ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indeed'])[2]/following::h5[1]</value>
      <webElementGuid>df6d5a7c-ab6b-484e-9e70-002e071e552c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Load more'])[2]/following::h5[2]</value>
      <webElementGuid>8d8e8525-8356-4217-9153-a8204eb13028</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='C++'])[1]/preceding::h5[1]</value>
      <webElementGuid>6904250c-b35d-45f4-ba70-e31e953246ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Java'])[1]/preceding::h5[2]</value>
      <webElementGuid>37f719a5-8cd1-47bf-908e-39a71527cb93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='C#']/parent::*</value>
      <webElementGuid>f141ad18-7f8e-4ee1-8630-ca81fed3c71f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[10]/div/div/h5</value>
      <webElementGuid>46e9c9aa-fa4f-416d-afc1-8f2319bf8b1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'C#' or . = 'C#')]</value>
      <webElementGuid>57f549d0-b2c1-4c5e-ac61-3d88aa45fd02</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
