<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Daftar Bootcamp (1)</name>
   <tag></tag>
   <elementGuidId>acb876a8-d641-41e6-8c77-c4160f08b4e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div[2]/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.daftarBootcamp.daftarhoverbootcamp</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e99993e1-a8c1-4a69-b0c6-4cfd0b1cb6bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bootcamp</name>
      <type>Main</type>
      <value>Quality Assurance Engineer Class</value>
      <webElementGuid>616a336f-f0a4-4a31-90d2-7af608c9f4ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>$store.dataForm.changeClassShow(2);$store.dataForm.classId = 4;$store.dataForm.changeClass()</value>
      <webElementGuid>5d233c49-45e8-4e95-9e4c-b2b7db1bd732</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>daftarBootcamp daftarhoverbootcamp</value>
      <webElementGuid>f77a1129-d8f2-4a52-9cc4-e784b7890f83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        Daftar Bootcamp
                                    </value>
      <webElementGuid>50ead436-1fd6-4106-ab22-934e706f583e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;bootcampBlock&quot;]/div[@class=&quot;blockBootcamp&quot;]/ul[@class=&quot;listBootcamp&quot;]/li[@class=&quot;listBootcampItem&quot;]/div[@class=&quot;tooltip-right&quot;]/button[@class=&quot;daftarBootcamp daftarhoverbootcamp&quot;]</value>
      <webElementGuid>0d1346fb-28c0-4eba-9eda-38315e714e5a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div[2]/button</value>
      <webElementGuid>847c74cc-95e1-4382-b870-a4aef0d80743</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pay Later'])[1]/following::button[1]</value>
      <webElementGuid>76b70f63-eaef-4e07-baa2-fcac240cb758</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BOOTCAMP'])[2]/preceding::button[1]</value>
      <webElementGuid>87766171-04cc-4091-a464-2b4ac5c7f273</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fullstack Engineer Class'])[3]/preceding::button[1]</value>
      <webElementGuid>c3806266-4db4-4e76-b764-eb4cdba7dd7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar Bootcamp']/parent::*</value>
      <webElementGuid>15f430ec-5c61-472e-8e33-7e598ebc6008</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div[2]/button</value>
      <webElementGuid>f0df0cfb-e89f-4d17-8ea5-04de2546f359</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                        Daftar Bootcamp
                                    ' or . = '
                                        Daftar Bootcamp
                                    ')]</value>
      <webElementGuid>9740df71-0ab2-461a-b62e-065c16db3358</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
