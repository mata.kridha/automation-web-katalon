<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Apakah kamu                            _b0b194</name>
   <tag></tag>
   <elementGuidId>c8b944ce-d4ef-481c-b62e-99919a857869</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e64f161d-5272-4edc-be3b-fdf39e697317</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-show</name>
      <type>Main</type>
      <value>!$store.dataForm.toDahsboard</value>
      <webElementGuid>d7c8a5dd-8477-4f19-98cd-825e67e5e09e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>block-input-bio</value>
      <webElementGuid>3db48785-b681-4e6a-9f79-e71ccda5e8ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
    
            
                    Javascript 
                
        

    



    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownskill', () => ({
            search: '',
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: '',
                value: ''
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == '') {
                    this.selected.label = '';
                    this.selected.value = '';
                    this.$dispatch('set-skill', '')
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.skill.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    '.skill.container-full-search')
                let containerShow = document.querySelector(
                    '.skill.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.skill.container-input-search')
                // let containerShow = document.querySelector(
                //     '.skill.container-full-search')
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';
                // containerShow.style.top = 'unset';



                // containerSearch.style.left = '24' + 'px';
                // containerSearch.style.right = '24' + 'px';
                // containerSearch.style.bottom = HEIGHT - fromtop + 'px';


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + 'px';
                // containerFullSearch.style.width = '100%';

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store('dataForm').skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch('set-skill', this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store('dataForm').skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, '');
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store('dataForm').skill = [...this.selectedArrary];
                    this.search = '';
                }
                //Alpine.store('dataForm').showOtherSkill = true;
                //Alpine.store('dataForm').readjustPosition();
                // let searchlenght = this.search.replace(/ /g, '');

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store('dataForm').activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == 'x' ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik '' diterapkan
                            

                            
                        </value>
      <webElementGuid>d6255d91-c62d-4770-a2d7-d5a52600abff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]</value>
      <webElementGuid>22df72d5-3856-4664-9e92-f6376521e6bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div</value>
      <webElementGuid>c5cd9027-0d76-44ea-b176-dfa63b0a48f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[7]/following::div[1]</value>
      <webElementGuid>a947b2f2-7454-4a80-b9ca-4d2fd93dce31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elka Verso'])[2]/following::div[1]</value>
      <webElementGuid>52d4cb04-e66a-4189-ab0e-8860eb051607</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div[3]/div/div[3]/div</value>
      <webElementGuid>1c9c4905-ba70-4be9-a5c8-b9a3fd9e9bb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
    
            
                    Javascript 
                
        

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownskill&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];
                    this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                }
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).showOtherSkill = true;
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).readjustPosition();
                // let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return &quot; , &quot;'&quot; , &quot;xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx&quot; , &quot;'&quot; , &quot;.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == &quot; , &quot;'&quot; , &quot;x&quot; , &quot;'&quot; , &quot; ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; diterapkan
                            

                            
                        &quot;) or . = concat(&quot;

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
    
            
                    Javascript 
                
        

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownskill&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];
                    this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                }
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).showOtherSkill = true;
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).readjustPosition();
                // let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return &quot; , &quot;'&quot; , &quot;xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx&quot; , &quot;'&quot; , &quot;.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == &quot; , &quot;'&quot; , &quot;x&quot; , &quot;'&quot; , &quot; ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; diterapkan
                            

                            
                        &quot;))]</value>
      <webElementGuid>868dccd9-71dc-4a36-9a28-a05f1c191f3c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
