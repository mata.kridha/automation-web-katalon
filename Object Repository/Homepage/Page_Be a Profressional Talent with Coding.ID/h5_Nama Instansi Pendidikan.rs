<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Nama Instansi Pendidikan</name>
   <tag></tag>
   <elementGuidId>b97527c2-e174-489e-b63b-07c4b82ff675</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.Instansi.dropdown.block-input-item > h5.new_body2_regular</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div[6]/div/h5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>da343691-d1d5-45a8-9448-998159327973</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body2_regular</value>
      <webElementGuid>73a3af1a-bb0f-494a-a646-a9d9029618da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Nama Instansi Pendidikan?*
    </value>
      <webElementGuid>8159e24a-b4ec-419a-80e4-68dd058d3e32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[6]/div[@class=&quot;Instansi dropdown block-input-item&quot;]/h5[@class=&quot;new_body2_regular&quot;]</value>
      <webElementGuid>4f539f6f-8451-45b3-8f18-1e0a39cd57bb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div[6]/div/h5</value>
      <webElementGuid>2730490e-4d4f-4320-bfb5-4302b13fdbb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[5]/following::h5[1]</value>
      <webElementGuid>f8f00eff-1f02-4e14-b41d-c8982ab4c3d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Instansi'])[1]/preceding::h5[1]</value>
      <webElementGuid>9c04bfe1-3cf9-4bb2-a3eb-a514ce9feb29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Nama Instansi Pendidikan?']/parent::*</value>
      <webElementGuid>c5436f7d-109b-4be7-902d-6b661a96d412</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/h5</value>
      <webElementGuid>e3755cda-644d-4493-8ef9-f0e3982efc51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Nama Instansi Pendidikan?*
    ' or . = 'Nama Instansi Pendidikan?*
    ')]</value>
      <webElementGuid>00cba6aa-46a5-4620-af51-4d534a057f62</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
