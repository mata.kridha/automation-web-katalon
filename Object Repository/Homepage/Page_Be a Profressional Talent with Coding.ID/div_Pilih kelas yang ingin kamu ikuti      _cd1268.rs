<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pilih kelas yang ingin kamu ikuti      _cd1268</name>
   <tag></tag>
   <elementGuidId>e525a23b-2f9a-4a36-a56a-dba61ea6f6d9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.main-form</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>cf50cb0b-e967-48b7-a8d3-53c0bf1dc731</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>main-form</value>
      <webElementGuid>9b9923a0-1afa-432a-8487-b124ac8d704a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    
                    
                        Pilih kelas yang ingin kamu ikuti

                        
                            
                            
                                

                                

                                


                                
                            
                        

                        
                            Pilih Kelas*

                            

                                
                                        
                                        Quality Assurance Engineer Class Tersedia
                                                Beasiswa
                                    
                                        
                                        Fullstack Engineer Class Tersedia
                                                Beasiswa
                                    
                                
                            
                        
                    

                    
                    
                        Hai, Elka Verso memilih Quality Assurance Engineer Class
                        Isi data berikut yuk, supaya
                            kamu dapat penawaran yang cocok untukmu
                        

                            
                                Nomor Whatsapp*
                                
                                
                                
                            


                            
    Domisili Saat Ini*
    
    
    
            Pilih Domisili
        
    
    


    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownDomisili', () => ({
            search: '',
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: '',
                value: ''
            },
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.domisili)
                    if (this.$store.dataForm.domisili == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }

                    if (this.$store.dataForm.domisiliFocus) {
                        this.showOptions();
                        this.$store.dataForm.domisiliFocus = false;


                        setTimeout(() => {
                            let searchFetch = document.getElementById(
                                'searchFetch');
                            searchFetch.focus();
                        }, 2000);

                        // console.log(&quot;show options&quot;)
                    } else {

                        // console.log(&quot;hide options&quot;)
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == '') {
                    this.selected.label = '';
                    this.selected.value = '';
                    this.$dispatch('set-domisili', '')
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.Domisili.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    '.Domisili.container-full-search')
                let containerShow = document.querySelector(
                    '.Domisili.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.Domisili.container-input-search')
                // let containerShow = document.querySelector(
                //     '.Domisili.container-full-search')
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';
                // containerShow.style.top = 'unset';



                // containerSearch.style.left = '24' + 'px';
                // containerSearch.style.right = '24' + 'px';
                // containerSearch.style.bottom = HEIGHT - fromtop + 'px';


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + 'px';
                // containerFullSearch.style.width = '100%';

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch('set-domisili', this.selected.value)
            },
            // addOther() {
            //     let option = {
            //         label: this.search,
            //         value: this.search
            //     }

            //     this.selected = option
            // },

        }))
    })

                            
                            

                            
                                Pendidikan
                                    Terakhir*
                                

                                
                                    
                                        
                                        Universitas/
                                            Sederajat
                                    

                                    
                                        
                                        Diploma
                                    

                                    
                                        
                                        SMA/SMK
                                    

                                    
                                        
                                        Yang
                                            lain
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Jurusan*
                                
                                
                            



                            
                                
    Nama Instansi Pendidikan?*
    
    
    
            Pilih Instansi
        
    

    



    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownInstansi', () => ({
            search: '',
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: '',
                value: ''
            },
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.instansi)
                    if (this.$store.dataForm.instansi == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == '') {
                    this.selected.label = '';
                    this.selected.value = '';
                    this.$dispatch('set-instansi', '')
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.Instansi.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    '.Instansi.container-full-search')
                let containerShow = document.querySelector(
                    '.Instansi.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.Instansi.container-input-search')
                // let containerShow = document.querySelector(
                //     '.Instansi.container-full-search')
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';
                // containerShow.style.top = 'unset';



                // containerSearch.style.left = '24' + 'px';
                // containerSearch.style.right = '24' + 'px';
                // containerSearch.style.bottom = HEIGHT - fromtop + 'px';


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + 'px';
                // containerFullSearch.style.width = '100%';

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch('set-instansi', this.selected.value)
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, '');

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },

        }))
    })

                                

                            

                            
                                Nama Instansi
                                    Pendidikan?*
                                
                                
                            

                            
                            
                            
                            

                            
                            
    Darimana kamu mengetahui Coding.ID?*
    
    

    
            Pilih Referensi
        
    
    
    

    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownReference', () => ({
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.reference)
                    if (this.$store.dataForm.reference == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }

                })

                // Alpine.effect(() => {
                //     if (this.$store.dataForm.domisilifocus) {
                //         this.showOptions();
                //         console.log(&quot;show options&quot;)
                //     } else {

                //         console.log(&quot;hide options&quot;)
                //     }
                // })

            },
            search: '',
            optionsVisible: false,
            selected: {
                label: '',
                value: ''
            },
            hideOptions() {
                this.optionsVisible = false

            },
            showOptions() {
                let HEIGHT = this.$refs.parentform.offsetHeight;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.Reference.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)

                let bottom = HEIGHT - fromtop + 57;;

                let containerFullSearch = document.querySelector(
                    '.Reference.container-full-search')
                let containerShow = document.querySelector(
                    '.Reference.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.Reference.container-input-search')

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // containerShow.style.bottom = bottom + 'px';
                // containerShow.style.top = 'unset';

                // containerShow.style.top = top + 'px';
                // console.log(left)
                // console.log(top)
                //console.log(bottom + &quot;bottom&quot;)
            },
            selectOption(option) {
                this.selected = option
                this.hideOptions()
                this.search = option.label
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, '');

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },
            options: [{&quot;label&quot;:&quot;POPFM&quot;,&quot;value&quot;:14},{&quot;label&quot;:&quot;Email Blast Coding.ID&quot;,&quot;value&quot;:13},{&quot;label&quot;:&quot;Coding.ID Mini Class\/ Webinar&quot;,&quot;value&quot;:12},{&quot;label&quot;:&quot;Kerabat\/ Teman&quot;,&quot;value&quot;:11},{&quot;label&quot;:&quot;Campus&quot;,&quot;value&quot;:10},{&quot;label&quot;:&quot;Indeed&quot;,&quot;value&quot;:9},{&quot;label&quot;:&quot;Jobstreet&quot;,&quot;value&quot;:8},{&quot;label&quot;:&quot;WhatsApp&quot;,&quot;value&quot;:7},{&quot;label&quot;:&quot;Youtube&quot;,&quot;value&quot;:6},{&quot;label&quot;:&quot;Discord&quot;,&quot;value&quot;:5},{&quot;label&quot;:&quot;Telegram&quot;,&quot;value&quot;:4},{&quot;label&quot;:&quot;Linked.In&quot;,&quot;value&quot;:3},{&quot;label&quot;:&quot;Facebook&quot;,&quot;value&quot;:2},{&quot;label&quot;:&quot;Instagram&quot;,&quot;value&quot;:1}],
            filterOptions() {

                return this.options.filter(option => {
                    return option.label.toLowerCase().includes(this.search.replace(/ /g, '')
                        .toLowerCase())
                })
            },
        }))
    })

                            
                            
                            
                        
                    

                    
                    

                        Hai, Elka Verso memilih
                            Quality Assurance Engineer Class
                        
                        Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.
                        

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
            Pilih skill
        
    

    



    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownskill', () => ({
            search: '',
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: '',
                value: ''
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == '') {
                    this.selected.label = '';
                    this.selected.value = '';
                    this.$dispatch('set-skill', '')
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.skill.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    '.skill.container-full-search')
                let containerShow = document.querySelector(
                    '.skill.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.skill.container-input-search')
                // let containerShow = document.querySelector(
                //     '.skill.container-full-search')
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';
                // containerShow.style.top = 'unset';



                // containerSearch.style.left = '24' + 'px';
                // containerSearch.style.right = '24' + 'px';
                // containerSearch.style.bottom = HEIGHT - fromtop + 'px';


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + 'px';
                // containerFullSearch.style.width = '100%';

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store('dataForm').skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch('set-skill', this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store('dataForm').skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, '');
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store('dataForm').skill = [...this.selectedArrary];
                    this.search = '';
                }
                //Alpine.store('dataForm').showOtherSkill = true;
                //Alpine.store('dataForm').readjustPosition();
                // let searchlenght = this.search.replace(/ /g, '');

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store('dataForm').activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == 'x' ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik '' diterapkan
                            

                            
                        




                        
                    


                
            </value>
      <webElementGuid>be5e4d92-f5eb-4828-826d-7af0486524a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]</value>
      <webElementGuid>8fbc5117-a5dc-4f11-80c3-3c682d11716e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]</value>
      <webElementGuid>eb9a3604-6a3e-4e34-bf41-915dc6d0c061</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pendaftaran Bootcamp'])[1]/following::div[7]</value>
      <webElementGuid>5815d55a-afbd-4216-bbe9-ff11b7235202</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Semua Events'])[1]/following::div[11]</value>
      <webElementGuid>8af06969-d039-483f-8669-132d5bc3f03c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div[3]</value>
      <webElementGuid>8551a2b3-8ba0-4f55-b7b2-d3480fb935da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
                
                    
                    
                        Pilih kelas yang ingin kamu ikuti

                        
                            
                            
                                

                                

                                


                                
                            
                        

                        
                            Pilih Kelas*

                            

                                
                                        
                                        Quality Assurance Engineer Class Tersedia
                                                Beasiswa
                                    
                                        
                                        Fullstack Engineer Class Tersedia
                                                Beasiswa
                                    
                                
                            
                        
                    

                    
                    
                        Hai, Elka Verso memilih Quality Assurance Engineer Class
                        Isi data berikut yuk, supaya
                            kamu dapat penawaran yang cocok untukmu
                        

                            
                                Nomor Whatsapp*
                                
                                
                                
                            


                            
    Domisili Saat Ini*
    
    
    
            Pilih Domisili
        
    
    


    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownDomisili&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.domisili)
                    if (this.$store.dataForm.domisili == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                    if (this.$store.dataForm.domisiliFocus) {
                        this.showOptions();
                        this.$store.dataForm.domisiliFocus = false;


                        setTimeout(() => {
                            let searchFetch = document.getElementById(
                                &quot; , &quot;'&quot; , &quot;searchFetch&quot; , &quot;'&quot; , &quot;);
                            searchFetch.focus();
                        }, 2000);

                        // console.log(&quot;show options&quot;)
                    } else {

                        // console.log(&quot;hide options&quot;)
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            // addOther() {
            //     let option = {
            //         label: this.search,
            //         value: this.search
            //     }

            //     this.selected = option
            // },

        }))
    })

                            
                            

                            
                                Pendidikan
                                    Terakhir*
                                

                                
                                    
                                        
                                        Universitas/
                                            Sederajat
                                    

                                    
                                        
                                        Diploma
                                    

                                    
                                        
                                        SMA/SMK
                                    

                                    
                                        
                                        Yang
                                            lain
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Jurusan*
                                
                                
                            



                            
                                
    Nama Instansi Pendidikan?*
    
    
    
            Pilih Instansi
        
    

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownInstansi&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.instansi)
                    if (this.$store.dataForm.instansi == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },

        }))
    })

                                

                            

                            
                                Nama Instansi
                                    Pendidikan?*
                                
                                
                            

                            
                            
                            
                            

                            
                            
    Darimana kamu mengetahui Coding.ID?*
    
    

    
            Pilih Referensi
        
    
    
    

    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownReference&quot; , &quot;'&quot; , &quot;, () => ({
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.reference)
                    if (this.$store.dataForm.reference == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                })

                // Alpine.effect(() => {
                //     if (this.$store.dataForm.domisilifocus) {
                //         this.showOptions();
                //         console.log(&quot;show options&quot;)
                //     } else {

                //         console.log(&quot;hide options&quot;)
                //     }
                // })

            },
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            optionsVisible: false,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            hideOptions() {
                this.optionsVisible = false

            },
            showOptions() {
                let HEIGHT = this.$refs.parentform.offsetHeight;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)

                let bottom = HEIGHT - fromtop + 57;;

                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-input-search&quot; , &quot;'&quot; , &quot;)

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.bottom = bottom + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.top = top + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // console.log(left)
                // console.log(top)
                //console.log(bottom + &quot;bottom&quot;)
            },
            selectOption(option) {
                this.selected = option
                this.hideOptions()
                this.search = option.label
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },
            options: [{&quot;label&quot;:&quot;POPFM&quot;,&quot;value&quot;:14},{&quot;label&quot;:&quot;Email Blast Coding.ID&quot;,&quot;value&quot;:13},{&quot;label&quot;:&quot;Coding.ID Mini Class\/ Webinar&quot;,&quot;value&quot;:12},{&quot;label&quot;:&quot;Kerabat\/ Teman&quot;,&quot;value&quot;:11},{&quot;label&quot;:&quot;Campus&quot;,&quot;value&quot;:10},{&quot;label&quot;:&quot;Indeed&quot;,&quot;value&quot;:9},{&quot;label&quot;:&quot;Jobstreet&quot;,&quot;value&quot;:8},{&quot;label&quot;:&quot;WhatsApp&quot;,&quot;value&quot;:7},{&quot;label&quot;:&quot;Youtube&quot;,&quot;value&quot;:6},{&quot;label&quot;:&quot;Discord&quot;,&quot;value&quot;:5},{&quot;label&quot;:&quot;Telegram&quot;,&quot;value&quot;:4},{&quot;label&quot;:&quot;Linked.In&quot;,&quot;value&quot;:3},{&quot;label&quot;:&quot;Facebook&quot;,&quot;value&quot;:2},{&quot;label&quot;:&quot;Instagram&quot;,&quot;value&quot;:1}],
            filterOptions() {

                return this.options.filter(option => {
                    return option.label.toLowerCase().includes(this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                        .toLowerCase())
                })
            },
        }))
    })

                            
                            
                            
                        
                    

                    
                    

                        Hai, Elka Verso memilih
                            Quality Assurance Engineer Class
                        
                        Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.
                        

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
            Pilih skill
        
    

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownskill&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];
                    this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                }
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).showOtherSkill = true;
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).readjustPosition();
                // let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return &quot; , &quot;'&quot; , &quot;xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx&quot; , &quot;'&quot; , &quot;.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == &quot; , &quot;'&quot; , &quot;x&quot; , &quot;'&quot; , &quot; ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; diterapkan
                            

                            
                        




                        
                    


                
            &quot;) or . = concat(&quot;
                
                    
                    
                        Pilih kelas yang ingin kamu ikuti

                        
                            
                            
                                

                                

                                


                                
                            
                        

                        
                            Pilih Kelas*

                            

                                
                                        
                                        Quality Assurance Engineer Class Tersedia
                                                Beasiswa
                                    
                                        
                                        Fullstack Engineer Class Tersedia
                                                Beasiswa
                                    
                                
                            
                        
                    

                    
                    
                        Hai, Elka Verso memilih Quality Assurance Engineer Class
                        Isi data berikut yuk, supaya
                            kamu dapat penawaran yang cocok untukmu
                        

                            
                                Nomor Whatsapp*
                                
                                
                                
                            


                            
    Domisili Saat Ini*
    
    
    
            Pilih Domisili
        
    
    


    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownDomisili&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.domisili)
                    if (this.$store.dataForm.domisili == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                    if (this.$store.dataForm.domisiliFocus) {
                        this.showOptions();
                        this.$store.dataForm.domisiliFocus = false;


                        setTimeout(() => {
                            let searchFetch = document.getElementById(
                                &quot; , &quot;'&quot; , &quot;searchFetch&quot; , &quot;'&quot; , &quot;);
                            searchFetch.focus();
                        }, 2000);

                        // console.log(&quot;show options&quot;)
                    } else {

                        // console.log(&quot;hide options&quot;)
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            // addOther() {
            //     let option = {
            //         label: this.search,
            //         value: this.search
            //     }

            //     this.selected = option
            // },

        }))
    })

                            
                            

                            
                                Pendidikan
                                    Terakhir*
                                

                                
                                    
                                        
                                        Universitas/
                                            Sederajat
                                    

                                    
                                        
                                        Diploma
                                    

                                    
                                        
                                        SMA/SMK
                                    

                                    
                                        
                                        Yang
                                            lain
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Jurusan*
                                
                                
                            



                            
                                
    Nama Instansi Pendidikan?*
    
    
    
            Pilih Instansi
        
    

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownInstansi&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.instansi)
                    if (this.$store.dataForm.instansi == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },

        }))
    })

                                

                            

                            
                                Nama Instansi
                                    Pendidikan?*
                                
                                
                            

                            
                            
                            
                            

                            
                            
    Darimana kamu mengetahui Coding.ID?*
    
    

    
            Pilih Referensi
        
    
    
    

    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownReference&quot; , &quot;'&quot; , &quot;, () => ({
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.reference)
                    if (this.$store.dataForm.reference == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                })

                // Alpine.effect(() => {
                //     if (this.$store.dataForm.domisilifocus) {
                //         this.showOptions();
                //         console.log(&quot;show options&quot;)
                //     } else {

                //         console.log(&quot;hide options&quot;)
                //     }
                // })

            },
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            optionsVisible: false,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            hideOptions() {
                this.optionsVisible = false

            },
            showOptions() {
                let HEIGHT = this.$refs.parentform.offsetHeight;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)

                let bottom = HEIGHT - fromtop + 57;;

                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-input-search&quot; , &quot;'&quot; , &quot;)

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.bottom = bottom + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.top = top + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // console.log(left)
                // console.log(top)
                //console.log(bottom + &quot;bottom&quot;)
            },
            selectOption(option) {
                this.selected = option
                this.hideOptions()
                this.search = option.label
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },
            options: [{&quot;label&quot;:&quot;POPFM&quot;,&quot;value&quot;:14},{&quot;label&quot;:&quot;Email Blast Coding.ID&quot;,&quot;value&quot;:13},{&quot;label&quot;:&quot;Coding.ID Mini Class\/ Webinar&quot;,&quot;value&quot;:12},{&quot;label&quot;:&quot;Kerabat\/ Teman&quot;,&quot;value&quot;:11},{&quot;label&quot;:&quot;Campus&quot;,&quot;value&quot;:10},{&quot;label&quot;:&quot;Indeed&quot;,&quot;value&quot;:9},{&quot;label&quot;:&quot;Jobstreet&quot;,&quot;value&quot;:8},{&quot;label&quot;:&quot;WhatsApp&quot;,&quot;value&quot;:7},{&quot;label&quot;:&quot;Youtube&quot;,&quot;value&quot;:6},{&quot;label&quot;:&quot;Discord&quot;,&quot;value&quot;:5},{&quot;label&quot;:&quot;Telegram&quot;,&quot;value&quot;:4},{&quot;label&quot;:&quot;Linked.In&quot;,&quot;value&quot;:3},{&quot;label&quot;:&quot;Facebook&quot;,&quot;value&quot;:2},{&quot;label&quot;:&quot;Instagram&quot;,&quot;value&quot;:1}],
            filterOptions() {

                return this.options.filter(option => {
                    return option.label.toLowerCase().includes(this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                        .toLowerCase())
                })
            },
        }))
    })

                            
                            
                            
                        
                    

                    
                    

                        Hai, Elka Verso memilih
                            Quality Assurance Engineer Class
                        
                        Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.
                        

                            
                                Apakah kamu
                                    memiliki basic programming?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                
    Apa bahasa pemrograman yang kamu kuasai?*
    
    
    
            Pilih skill
        
    

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownskill&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            selectedArrary: [],
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.skill)
                    if (this.$store.dataForm.skill == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getcode?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.code,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.skill.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.skill.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option
                // console.log(first)
                //this.search = option.label


                console.log(option);

                if (this.selectedArrary.length > 0) {
                    const checkIdCode = obj => obj.value == option.value;
                    let checkExist = this.selectedArrary.some(checkIdCode);

                    if (!checkExist) {
                        let tempArray = this.selectedArrary;
                        tempArray.push({
                            &quot;label&quot;: option.label,
                            &quot;value&quot;: option.value
                        });
                        this.selectedArrary = tempArray;

                        console.log(tempArray)
                    }
                } else {
                    let tempArray = this.selectedArrary;
                    console.log(option)
                    tempArray.push({
                        &quot;label&quot;: option.label,
                        &quot;value&quot;: option.value
                    });
                    this.selectedArrary = tempArray;
                    console.log(tempArray)
                }
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];

                this.hideOptions()
                //this.$dispatch(&quot; , &quot;'&quot; , &quot;set-skill&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            deleteSelect(value) {
                let tempArray = this.selectedArrary;

                const index = tempArray.findIndex(object => {
                    return object.value == value;
                });


                tempArray.splice(index, 1);
                this.selectedArrary = tempArray;
                Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = tempArray;
            },
            addOther() {
                let data = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
                if (data.length > 0) {

                    // console.log(first)
                    //this.search = option.label



                    if (this.selectedArrary.length > 0) {
                        const checkIdCode = obj => obj.label.toLowerCase() == this.search.trim()
                            .toLowerCase();
                        let checkExist = this.selectedArrary.some(checkIdCode);

                        if (!checkExist) {
                            let tempArray = this.selectedArrary;
                            tempArray.push({
                                &quot;label&quot;: this.search.trim(),
                                &quot;value&quot;: this.search.trim()
                            });
                            this.selectedArrary = tempArray;

                            console.log(tempArray)
                        }
                    } else {
                        let tempArray = this.selectedArrary;

                        tempArray.push({
                            &quot;label&quot;: this.search.trim(),
                            &quot;value&quot;: this.search.trim()
                        });
                        this.selectedArrary = tempArray;
                        console.log(tempArray)
                    }
                    Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).skill = [...this.selectedArrary];
                    this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                }
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).showOtherSkill = true;
                //Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).readjustPosition();
                // let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                // if (searchlenght.length > 0) {
                //     let option = {
                //         label: this.search,
                //         value: this.search
                //     }

                //     this.selected = option;
                // }

                this.optionsVisible = false;
                changeAllTheHeight()
                // let pages = document.querySelectorAll(&quot;.page&quot;);
                // // console.log(this.activePage)
                // let activePage = Alpine.store(&quot; , &quot;'&quot; , &quot;dataForm&quot; , &quot;'&quot; , &quot;).activePage
                // const slideWidth = pages[activePage].getBoundingClientRect().width;

                // const trackForm = document.querySelector(&quot;.track-form&quot;);
                // trackForm.style.transform = `translateX(-${slideWidth * activePage}px)`;
                // const slideHeight = pages[activePage].getBoundingClientRect().height;
                // containerPage.style.height = `${slideHeight}px`;

                // pages.forEach((page, index) => {
                //     page.style.opacity = &quot;1&quot;;
                //     page.style.left = `${slideWidth * index}px`;
                // });

                // let tooltipLevel = document.querySelector(&quot;.tooltip-level&quot;);
                // let boxLevel = document.querySelector(&quot;.level-box&quot;);
                // if (boxLevel) {
                //     tooltipLevel.style.width = `${boxLevel.offsetWidth}px`;
                // }
            },
            uuid() {
                return &quot; , &quot;'&quot; , &quot;xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx&quot; , &quot;'&quot; , &quot;.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == &quot; , &quot;'&quot; , &quot;x&quot; , &quot;'&quot; , &quot; ? r : (r &amp; 0x3 | 0x8);
                    return v.toString(16);
                });
            },

        }))
    })

                                

                            

                            


                            
                                Apa motivasi
                                    kamu
                                    mengikuti bootcamp?*
                                
                                
                            

                            
                                Apakah kamu
                                    saat
                                    ini
                                    sedang bekerja?*

                                
                                    
                                        
                                        Tidak
                                    

                                    
                                        
                                        Ya
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Apakah kamu
                                    bersedia ditempatkan kerja dimana saja setelah lulus dari Coding.Id?*

                                

                                    
                                        
                                        Ya
                                    
                                    
                                        
                                        Tidak
                                    
                                
                            

                            
                                Kode Refferal
                                    (Optional)
                                
                                
                                Kode milik &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; diterapkan
                            

                            
                        




                        
                    


                
            &quot;))]</value>
      <webElementGuid>ab80e086-73a8-43a8-b9bc-66e78e1aa76c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
