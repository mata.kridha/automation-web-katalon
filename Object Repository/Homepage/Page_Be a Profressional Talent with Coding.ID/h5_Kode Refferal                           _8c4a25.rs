<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Kode Refferal                           _8c4a25</name>
   <tag></tag>
   <elementGuidId>b24a4158-6ccb-4c0b-be07-50d1f1ebf4ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div[6]/h5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>c1a4f579-f16d-4db8-ac35-b61190b78dd3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body2_regular</value>
      <webElementGuid>8fbc7ef9-2c96-4db6-9b8b-43c6441443f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Kode Refferal
                                    (Optional)</value>
      <webElementGuid>81a98588-3196-4cdb-9170-238667bca9c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[@class=&quot;block-input-item&quot;]/h5[@class=&quot;new_body2_regular&quot;]</value>
      <webElementGuid>38f572d4-d8cf-4658-b5c2-3a5e944797a0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div[6]/h5</value>
      <webElementGuid>ab2b24f7-b467-4d5b-9726-700c9f2edcb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tidak'])[3]/following::h5[1]</value>
      <webElementGuid>908f1e0c-4eaf-456d-947a-6f1fbacac840</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ya'])[3]/following::h5[1]</value>
      <webElementGuid>80f71d88-b2dd-4ec7-af4d-a5a02612195f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kode tidak valid'])[1]/preceding::h5[1]</value>
      <webElementGuid>1a310fd3-7ec7-4f82-a8f4-84553f6b06d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Kode milik ', &quot;'&quot;, '', &quot;'&quot;, ' diterapkan')])[1]/preceding::h5[1]</value>
      <webElementGuid>2f4fa1d8-c621-4d27-8a75-cb435010b97f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/h5</value>
      <webElementGuid>23c56a9b-80a5-4a8e-bac9-15a3d00ca225</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Kode Refferal
                                    (Optional)' or . = 'Kode Refferal
                                    (Optional)')]</value>
      <webElementGuid>fc6aa4da-f354-41c5-a9b9-5fe515344617</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
