<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Nomor Whatsapp                         _32df20</name>
   <tag></tag>
   <elementGuidId>05836836-e549-481b-9559-f440f157fb1a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.block-input-bio</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a803087a-308c-4e94-a85a-d4e01eba9107</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>block-input-bio</value>
      <webElementGuid>ba6c738c-b89f-47ef-82dc-732158d14d45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

                            
                                Nomor Whatsapp*
                                
                                
                                
                            


                            
    Domisili Saat Ini*
    
    
    
    KAB TIMOR TENGAH SELATAN
    


    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownDomisili', () => ({
            search: '',
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: '',
                value: ''
            },
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.domisili)
                    if (this.$store.dataForm.domisili == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }

                    if (this.$store.dataForm.domisiliFocus) {
                        this.showOptions();
                        this.$store.dataForm.domisiliFocus = false;


                        setTimeout(() => {
                            let searchFetch = document.getElementById(
                                'searchFetch');
                            searchFetch.focus();
                        }, 2000);

                        // console.log(&quot;show options&quot;)
                    } else {

                        // console.log(&quot;hide options&quot;)
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == '') {
                    this.selected.label = '';
                    this.selected.value = '';
                    this.$dispatch('set-domisili', '')
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.Domisili.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    '.Domisili.container-full-search')
                let containerShow = document.querySelector(
                    '.Domisili.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.Domisili.container-input-search')
                // let containerShow = document.querySelector(
                //     '.Domisili.container-full-search')
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';
                // containerShow.style.top = 'unset';



                // containerSearch.style.left = '24' + 'px';
                // containerSearch.style.right = '24' + 'px';
                // containerSearch.style.bottom = HEIGHT - fromtop + 'px';


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + 'px';
                // containerFullSearch.style.width = '100%';

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch('set-domisili', this.selected.value)
            },
            // addOther() {
            //     let option = {
            //         label: this.search,
            //         value: this.search
            //     }

            //     this.selected = option
            // },

        }))
    })

                            
                            

                            
                                Pendidikan
                                    Terakhir*
                                

                                
                                    
                                        
                                        Universitas/
                                            Sederajat
                                    

                                    
                                        
                                        Diploma
                                    

                                    
                                        
                                        SMA/SMK
                                    

                                    
                                        
                                        Yang
                                            lain
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Jurusan*
                                
                                
                            



                            
                                
    Nama Instansi Pendidikan?*
    
    
    
            Pilih Instansi
        
    

    



    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownInstansi', () => ({
            search: '',
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: '',
                value: ''
            },
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.instansi)
                    if (this.$store.dataForm.instansi == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == '') {
                    this.selected.label = '';
                    this.selected.value = '';
                    this.$dispatch('set-instansi', '')
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.Instansi.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    '.Instansi.container-full-search')
                let containerShow = document.querySelector(
                    '.Instansi.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.Instansi.container-input-search')
                // let containerShow = document.querySelector(
                //     '.Instansi.container-full-search')
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';
                // containerShow.style.top = 'unset';



                // containerSearch.style.left = '24' + 'px';
                // containerSearch.style.right = '24' + 'px';
                // containerSearch.style.bottom = HEIGHT - fromtop + 'px';


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + 'px';
                // containerFullSearch.style.width = '100%';

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch('set-instansi', this.selected.value)
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, '');

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },

        }))
    })

                                

                            

                            
                                Nama Instansi
                                    Pendidikan?*
                                
                                
                            

                            
                            
                            
                            

                            
                            
    Darimana kamu mengetahui Coding.ID?*
    
    

    
            Pilih Referensi
        
    
    
    

    





    document.addEventListener('alpine:init', () => {
        Alpine.data('dropdownReference', () => ({
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.reference)
                    if (this.$store.dataForm.reference == '') {
                        this.selected.label = '';
                        this.selected.value = '';
                        this.search = '';
                    }

                })

                // Alpine.effect(() => {
                //     if (this.$store.dataForm.domisilifocus) {
                //         this.showOptions();
                //         console.log(&quot;show options&quot;)
                //     } else {

                //         console.log(&quot;hide options&quot;)
                //     }
                // })

            },
            search: '',
            optionsVisible: false,
            selected: {
                label: '',
                value: ''
            },
            hideOptions() {
                this.optionsVisible = false

            },
            showOptions() {
                let HEIGHT = this.$refs.parentform.offsetHeight;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById('FormBootcampContainer');
                let inputBlockDropdown = document.querySelector(
                    '.Reference.dropdown.block-input-item');
                let headerForm = document.querySelector('.header-bootcamp');
                let stepForm = document.querySelector('.step-form-block');

                let footerBlock = document.querySelector('.footer-form-block');


                inputBlockDropdown.style.position = 'unset';
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)

                let bottom = HEIGHT - fromtop + 57;;

                let containerFullSearch = document.querySelector(
                    '.Reference.container-full-search')
                let containerShow = document.querySelector(
                    '.Reference.container-show-dropdown')
                let containerSearch = document.querySelector(
                    '.Reference.container-input-search')

                containerFullSearch.style.bottom = HEIGHT - fromtop + 'px';
                containerFullSearch.style.left = '24' + 'px';
                containerFullSearch.style.right = '24' + 'px';

                // containerShow.style.left = '24' + 'px';
                // containerShow.style.right = '24' + 'px';

                // containerShow.style.bottom = bottom + 'px';
                // containerShow.style.top = 'unset';

                // containerShow.style.top = top + 'px';
                // console.log(left)
                // console.log(top)
                //console.log(bottom + &quot;bottom&quot;)
            },
            selectOption(option) {
                this.selected = option
                this.hideOptions()
                this.search = option.label
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, '');

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },
            options: [{&quot;label&quot;:&quot;POPFM&quot;,&quot;value&quot;:14},{&quot;label&quot;:&quot;Email Blast Coding.ID&quot;,&quot;value&quot;:13},{&quot;label&quot;:&quot;Coding.ID Mini Class\/ Webinar&quot;,&quot;value&quot;:12},{&quot;label&quot;:&quot;Kerabat\/ Teman&quot;,&quot;value&quot;:11},{&quot;label&quot;:&quot;Campus&quot;,&quot;value&quot;:10},{&quot;label&quot;:&quot;Indeed&quot;,&quot;value&quot;:9},{&quot;label&quot;:&quot;Jobstreet&quot;,&quot;value&quot;:8},{&quot;label&quot;:&quot;WhatsApp&quot;,&quot;value&quot;:7},{&quot;label&quot;:&quot;Youtube&quot;,&quot;value&quot;:6},{&quot;label&quot;:&quot;Discord&quot;,&quot;value&quot;:5},{&quot;label&quot;:&quot;Telegram&quot;,&quot;value&quot;:4},{&quot;label&quot;:&quot;Linked.In&quot;,&quot;value&quot;:3},{&quot;label&quot;:&quot;Facebook&quot;,&quot;value&quot;:2},{&quot;label&quot;:&quot;Instagram&quot;,&quot;value&quot;:1}],
            filterOptions() {

                return this.options.filter(option => {
                    return option.label.toLowerCase().includes(this.search.replace(/ /g, '')
                        .toLowerCase())
                })
            },
        }))
    })

                            
                            
                            
                        </value>
      <webElementGuid>83886d3c-5987-4e2a-8a74-1dff1df395e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]</value>
      <webElementGuid>32471405-56e5-4961-8269-1c8610fa3a64</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div</value>
      <webElementGuid>da8833b8-173e-40e1-8ebb-73a5f3334691</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[6]/following::div[1]</value>
      <webElementGuid>84d0b11a-1ae1-4cd5-8e0c-da31ae0b7a0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elka Verso'])[1]/following::div[1]</value>
      <webElementGuid>45678ce9-492b-4dfc-a620-e5617a171e4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div[3]/div/div[2]/div</value>
      <webElementGuid>55a45b7d-f070-436b-8364-b5c49183050e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;

                            
                                Nomor Whatsapp*
                                
                                
                                
                            


                            
    Domisili Saat Ini*
    
    
    
    KAB TIMOR TENGAH SELATAN
    


    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownDomisili&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.domisili)
                    if (this.$store.dataForm.domisili == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                    if (this.$store.dataForm.domisiliFocus) {
                        this.showOptions();
                        this.$store.dataForm.domisiliFocus = false;


                        setTimeout(() => {
                            let searchFetch = document.getElementById(
                                &quot; , &quot;'&quot; , &quot;searchFetch&quot; , &quot;'&quot; , &quot;);
                            searchFetch.focus();
                        }, 2000);

                        // console.log(&quot;show options&quot;)
                    } else {

                        // console.log(&quot;hide options&quot;)
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            // addOther() {
            //     let option = {
            //         label: this.search,
            //         value: this.search
            //     }

            //     this.selected = option
            // },

        }))
    })

                            
                            

                            
                                Pendidikan
                                    Terakhir*
                                

                                
                                    
                                        
                                        Universitas/
                                            Sederajat
                                    

                                    
                                        
                                        Diploma
                                    

                                    
                                        
                                        SMA/SMK
                                    

                                    
                                        
                                        Yang
                                            lain
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Jurusan*
                                
                                
                            



                            
                                
    Nama Instansi Pendidikan?*
    
    
    
            Pilih Instansi
        
    

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownInstansi&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.instansi)
                    if (this.$store.dataForm.instansi == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },

        }))
    })

                                

                            

                            
                                Nama Instansi
                                    Pendidikan?*
                                
                                
                            

                            
                            
                            
                            

                            
                            
    Darimana kamu mengetahui Coding.ID?*
    
    

    
            Pilih Referensi
        
    
    
    

    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownReference&quot; , &quot;'&quot; , &quot;, () => ({
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.reference)
                    if (this.$store.dataForm.reference == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                })

                // Alpine.effect(() => {
                //     if (this.$store.dataForm.domisilifocus) {
                //         this.showOptions();
                //         console.log(&quot;show options&quot;)
                //     } else {

                //         console.log(&quot;hide options&quot;)
                //     }
                // })

            },
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            optionsVisible: false,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            hideOptions() {
                this.optionsVisible = false

            },
            showOptions() {
                let HEIGHT = this.$refs.parentform.offsetHeight;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)

                let bottom = HEIGHT - fromtop + 57;;

                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-input-search&quot; , &quot;'&quot; , &quot;)

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.bottom = bottom + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.top = top + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // console.log(left)
                // console.log(top)
                //console.log(bottom + &quot;bottom&quot;)
            },
            selectOption(option) {
                this.selected = option
                this.hideOptions()
                this.search = option.label
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },
            options: [{&quot;label&quot;:&quot;POPFM&quot;,&quot;value&quot;:14},{&quot;label&quot;:&quot;Email Blast Coding.ID&quot;,&quot;value&quot;:13},{&quot;label&quot;:&quot;Coding.ID Mini Class\/ Webinar&quot;,&quot;value&quot;:12},{&quot;label&quot;:&quot;Kerabat\/ Teman&quot;,&quot;value&quot;:11},{&quot;label&quot;:&quot;Campus&quot;,&quot;value&quot;:10},{&quot;label&quot;:&quot;Indeed&quot;,&quot;value&quot;:9},{&quot;label&quot;:&quot;Jobstreet&quot;,&quot;value&quot;:8},{&quot;label&quot;:&quot;WhatsApp&quot;,&quot;value&quot;:7},{&quot;label&quot;:&quot;Youtube&quot;,&quot;value&quot;:6},{&quot;label&quot;:&quot;Discord&quot;,&quot;value&quot;:5},{&quot;label&quot;:&quot;Telegram&quot;,&quot;value&quot;:4},{&quot;label&quot;:&quot;Linked.In&quot;,&quot;value&quot;:3},{&quot;label&quot;:&quot;Facebook&quot;,&quot;value&quot;:2},{&quot;label&quot;:&quot;Instagram&quot;,&quot;value&quot;:1}],
            filterOptions() {

                return this.options.filter(option => {
                    return option.label.toLowerCase().includes(this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                        .toLowerCase())
                })
            },
        }))
    })

                            
                            
                            
                        &quot;) or . = concat(&quot;

                            
                                Nomor Whatsapp*
                                
                                
                                
                            


                            
    Domisili Saat Ini*
    
    
    
    KAB TIMOR TENGAH SELATAN
    


    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownDomisili&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.domisili)
                    if (this.$store.dataForm.domisili == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                    if (this.$store.dataForm.domisiliFocus) {
                        this.showOptions();
                        this.$store.dataForm.domisiliFocus = false;


                        setTimeout(() => {
                            let searchFetch = document.getElementById(
                                &quot; , &quot;'&quot; , &quot;searchFetch&quot; , &quot;'&quot; , &quot;);
                            searchFetch.focus();
                        }, 2000);

                        // console.log(&quot;show options&quot;)
                    } else {

                        // console.log(&quot;hide options&quot;)
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getwilayah?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.kode,
                                label: wilayah.nama
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Domisili.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Domisili.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-domisili&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            // addOther() {
            //     let option = {
            //         label: this.search,
            //         value: this.search
            //     }

            //     this.selected = option
            // },

        }))
    })

                            
                            

                            
                                Pendidikan
                                    Terakhir*
                                

                                
                                    
                                        
                                        Universitas/
                                            Sederajat
                                    

                                    
                                        
                                        Diploma
                                    

                                    
                                        
                                        SMA/SMK
                                    

                                    
                                        
                                        Yang
                                            lain
                                        
                                            
                                            
                                        
                                    
                                
                            

                            
                                Jurusan*
                                
                                
                            



                            
                                
    Nama Instansi Pendidikan?*
    
    
    
            Pilih Instansi
        
    

    



    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownInstansi&quot; , &quot;'&quot; , &quot;, () => ({
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            options: [],
            isLoading: false,
            optionsVisible: false,
            totalPage: 0,
            currentPage: 0,
            totalData: 0,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            init() {
                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.instansi)
                    if (this.$store.dataForm.instansi == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }
                })

            },
            loadmore() {

                this.optionsVisible.true;
                // this.$refs.inputfocus.focus();
                this.isLoading = true;

                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                        page: this.currentPage + 1
                    }))
                    .then(res => res.json())
                    .then(data => {

                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = [...this.options, ...result];

                        }
                    });
            },
            fetchData() {
                this.isLoading = true;
                this.options = [];
                fetch(`https://demo-app.online/api/getuniversities?` + new URLSearchParams({
                        q: this.search,
                    }))
                    .then(res => res.json())
                    .then(data => {
                        //console.log(data)
                        this.isLoading = false;
                        // this.pokemon = data;

                        if (data.data) {
                            // this.options = data.data;
                            this.totalPage = data.last_page;
                            this.currentPage = data.current_page;
                            this.totalData = data.total;

                            let result = data.data.map(wilayah => ({
                                value: wilayah.id,
                                label: wilayah.name
                            }));

                            this.options = result;

                        }
                    });
            },
            hideOptions() {
                this.optionsVisible = false;
                //console.log(&quot;hide options&quot;);

                if (this.options.length == 0 || this.search == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                    this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                    // console.log(&quot;dispatch empty domisili&quot;);
                }

            },
            showOptions() {
                if (this.search.length == 0 &amp;&amp; this.isLoading == false) {
                    this.fetchData();
                }

                let HEIGHT = this.$refs.parentform.offsetHeight;

                //console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)



                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Instansi.container-input-search&quot; , &quot;'&quot; , &quot;)
                // let containerShow = document.querySelector(
                //     &quot; , &quot;'&quot; , &quot;.Instansi.container-full-search&quot; , &quot;'&quot; , &quot;)
                let bottom = HEIGHT - fromtop + 57;
                //let bottom = HEIGHT - fromtop + containerSearch.offsetHeight;

                //console.log(containerSearch.hei + &quot;this is containerSearch&quot;);

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);

                // console.log(fromtop + &quot;this is fromtop&quot;);

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;



                // containerSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;


                // let myDivWidth = containerShow.clientWidth || containerShow.offsetWidth || (
                //     containerShow
                //     .getBoundingClientRect()).width;
                // let searchHeight = containerSearch.clientHeight || containerSearch.offsetHeight || (
                //     containerSearch
                //     .getBoundingClientRect()).height;

                // let containerShowHeight = containerShow.clientHeight || containerShow
                //     .offsetHeight || (containerShow
                //         .getBoundingClientRect()).height;

                // containerFullSearch.style.height = searchHeight + containerShowHeight + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerFullSearch.style.width = &quot; , &quot;'&quot; , &quot;100%&quot; , &quot;'&quot; , &quot;;

                this.optionsVisible = true;

            },
            selectOption(option) {
                this.selected = option

                this.search = option.label
                this.hideOptions()
                this.$dispatch(&quot; , &quot;'&quot; , &quot;set-instansi&quot; , &quot;'&quot; , &quot;, this.selected.value)
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },

        }))
    })

                                

                            

                            
                                Nama Instansi
                                    Pendidikan?*
                                
                                
                            

                            
                            
                            
                            

                            
                            
    Darimana kamu mengetahui Coding.ID?*
    
    

    
            Pilih Referensi
        
    
    
    

    





    document.addEventListener(&quot; , &quot;'&quot; , &quot;alpine:init&quot; , &quot;'&quot; , &quot;, () => {
        Alpine.data(&quot; , &quot;'&quot; , &quot;dropdownReference&quot; , &quot;'&quot; , &quot;, () => ({
            init() {

                Alpine.effect(() => {
                    // console.log(this.$store.dataForm.reference)
                    if (this.$store.dataForm.reference == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                        this.selected.label = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.selected.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                        this.search = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                    }

                })

                // Alpine.effect(() => {
                //     if (this.$store.dataForm.domisilifocus) {
                //         this.showOptions();
                //         console.log(&quot;show options&quot;)
                //     } else {

                //         console.log(&quot;hide options&quot;)
                //     }
                // })

            },
            search: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
            optionsVisible: false,
            selected: {
                label: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
                value: &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
            },
            hideOptions() {
                this.optionsVisible = false

            },
            showOptions() {
                let HEIGHT = this.$refs.parentform.offsetHeight;

                // console.log(HEIGHT + &quot;this is height of parent&quot;);
                this.optionsVisible = true;
                let containerForm = document.getElementById(&quot; , &quot;'&quot; , &quot;FormBootcampContainer&quot; , &quot;'&quot; , &quot;);
                let inputBlockDropdown = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.dropdown.block-input-item&quot; , &quot;'&quot; , &quot;);
                let headerForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.header-bootcamp&quot; , &quot;'&quot; , &quot;);
                let stepForm = document.querySelector(&quot; , &quot;'&quot; , &quot;.step-form-block&quot; , &quot;'&quot; , &quot;);

                let footerBlock = document.querySelector(&quot; , &quot;'&quot; , &quot;.footer-form-block&quot; , &quot;'&quot; , &quot;);


                inputBlockDropdown.style.position = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;
                let left = inputBlockDropdown.offsetLeft;
                // let top = inputBlockDropdown.offsetTop + inputBlockDropdown.offsetHeight +
                //     headerForm.offsetHeight + stepForm.offsetHeight;

                // let top = inputBlockDropdown.offsetTop +
                //     headerForm.offsetHeight + stepForm.offsetHeight - 24;

                //let bottom = footerBlock.offsetHeight + inputBlockDropdown.offsetHeight + 24;
                // console.log(inputBlockDropdown.top + &quot;this is top&quot;);
                // console.log(inputBlockDropdown.bottom + &quot;this is bottom&quot;);
                let fromtop = inputBlockDropdown.offsetTop + headerForm.offsetHeight + stepForm
                    .offsetHeight;
                // console.log(inputBlockDropdown.offsetTop + &quot;fromtop&quot;)
                // console.log(inputBlockDropdown.offsetHeight + &quot;fromHeight&quot;)

                let bottom = HEIGHT - fromtop + 57;;

                let containerFullSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-full-search&quot; , &quot;'&quot; , &quot;)
                let containerShow = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-show-dropdown&quot; , &quot;'&quot; , &quot;)
                let containerSearch = document.querySelector(
                    &quot; , &quot;'&quot; , &quot;.Reference.container-input-search&quot; , &quot;'&quot; , &quot;)

                containerFullSearch.style.bottom = HEIGHT - fromtop + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                containerFullSearch.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.left = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.right = &quot; , &quot;'&quot; , &quot;24&quot; , &quot;'&quot; , &quot; + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.bottom = bottom + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // containerShow.style.top = &quot; , &quot;'&quot; , &quot;unset&quot; , &quot;'&quot; , &quot;;

                // containerShow.style.top = top + &quot; , &quot;'&quot; , &quot;px&quot; , &quot;'&quot; , &quot;;
                // console.log(left)
                // console.log(top)
                //console.log(bottom + &quot;bottom&quot;)
            },
            selectOption(option) {
                this.selected = option
                this.hideOptions()
                this.search = option.label
            },
            addOther() {

                let searchlenght = this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);

                if (searchlenght.length > 0) {
                    let option = {
                        label: this.search,
                        value: this.search
                    }

                    this.selected = option;
                }

                this.optionsVisible = false;
            },
            options: [{&quot;label&quot;:&quot;POPFM&quot;,&quot;value&quot;:14},{&quot;label&quot;:&quot;Email Blast Coding.ID&quot;,&quot;value&quot;:13},{&quot;label&quot;:&quot;Coding.ID Mini Class\/ Webinar&quot;,&quot;value&quot;:12},{&quot;label&quot;:&quot;Kerabat\/ Teman&quot;,&quot;value&quot;:11},{&quot;label&quot;:&quot;Campus&quot;,&quot;value&quot;:10},{&quot;label&quot;:&quot;Indeed&quot;,&quot;value&quot;:9},{&quot;label&quot;:&quot;Jobstreet&quot;,&quot;value&quot;:8},{&quot;label&quot;:&quot;WhatsApp&quot;,&quot;value&quot;:7},{&quot;label&quot;:&quot;Youtube&quot;,&quot;value&quot;:6},{&quot;label&quot;:&quot;Discord&quot;,&quot;value&quot;:5},{&quot;label&quot;:&quot;Telegram&quot;,&quot;value&quot;:4},{&quot;label&quot;:&quot;Linked.In&quot;,&quot;value&quot;:3},{&quot;label&quot;:&quot;Facebook&quot;,&quot;value&quot;:2},{&quot;label&quot;:&quot;Instagram&quot;,&quot;value&quot;:1}],
            filterOptions() {

                return this.options.filter(option => {
                    return option.label.toLowerCase().includes(this.search.replace(/ /g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                        .toLowerCase())
                })
            },
        }))
    })

                            
                            
                            
                        &quot;))]</value>
      <webElementGuid>3cd98908-52c2-412d-a5ab-11013cfc0475</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
