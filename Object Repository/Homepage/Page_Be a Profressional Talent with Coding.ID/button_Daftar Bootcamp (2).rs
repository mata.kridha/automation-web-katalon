<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Daftar Bootcamp (2)</name>
   <tag></tag>
   <elementGuidId>28036999-70ad-41bc-baf7-49d0f73a2e44</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#next-button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='next-button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>63c0ca7e-2f20-4f99-872f-caa64f96b91f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bootcamp</name>
      <type>Main</type>
      <value>Quality Assurance Engineer Class</value>
      <webElementGuid>edb2d8cf-1e52-4c15-b569-8a4ed1d39386</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>$store.dataForm.changeClassShow(2);$store.dataForm.classId = 4;$store.dataForm.changeClass()</value>
      <webElementGuid>3ab10b97-ec54-43ff-8c4c-09850d50e801</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>daftarBootcamp daftarBootcamp--see-all disabled</value>
      <webElementGuid>e424ae46-088f-496d-8731-946ec8c91f89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Daftar Bootcamp</value>
      <webElementGuid>90adc6a2-d21b-4d19-a611-9a3234bf68be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;next-button&quot;)</value>
      <webElementGuid>1abc1331-d4a0-4584-927f-34ebd905911c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-on:click</name>
      <type>Main</type>
      <value>$store.dataForm.validatePage($store.dataForm.activePage)</value>
      <webElementGuid>4b11462f-82be-48e6-98f6-572af4d1284f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>next-button</value>
      <webElementGuid>07203c63-6a1f-4a73-9a83-4a398f389000</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>:class</name>
      <type>Main</type>
      <value>$store.dataForm.classId == 0 ||
                            ($store.dataForm.activePage == 1 &amp;&amp; !$store.dataForm.validPage2) || ($store.dataForm
                                .activePage == 2 &amp;&amp; !$store.dataForm.validPage3) ?
                            'disabled' : ''</value>
      <webElementGuid>3ca0b6b3-286e-459a-aa73-0c7bf382ce52</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='next-button']</value>
      <webElementGuid>4b793855-dd6c-4ae7-8f22-39bb1d4f90dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[4]/div/button[2]</value>
      <webElementGuid>732e9c01-14f0-41a3-a625-42a585261962</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sebelumnya'])[1]/following::button[1]</value>
      <webElementGuid>94e99ba7-b239-4d4c-91f3-275d8f38a32e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Kode milik ', &quot;'&quot;, '', &quot;'&quot;, ' diterapkan')])[1]/following::button[2]</value>
      <webElementGuid>c8307021-e3d4-4d40-8c87-af2090cb928c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Whatsapp'])[1]/preceding::button[1]</value>
      <webElementGuid>9454d2f5-43d1-45cb-aeb9-7a61e43e6958</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Perhatian'])[1]/preceding::button[1]</value>
      <webElementGuid>c3238879-6100-4f33-919f-a7924a7f195b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/button[2]</value>
      <webElementGuid>b6d41f1d-c6eb-4c0f-ae31-4610c6861b21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'next-button' and (text() = 'Daftar Bootcamp' or . = 'Daftar Bootcamp')]</value>
      <webElementGuid>f4b9df10-c8c3-4f48-9bb8-3459e1b68b26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div[2]/button</value>
      <webElementGuid>b0c0c011-604f-4008-b19f-6c3066c6da69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pay Later'])[1]/following::button[1]</value>
      <webElementGuid>19fae84e-cf36-412d-91a0-b1369a2318be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BOOTCAMP'])[2]/preceding::button[1]</value>
      <webElementGuid>e264ab25-2eef-4620-8d43-2a41ae042df0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fullstack Engineer Class'])[3]/preceding::button[1]</value>
      <webElementGuid>e833d1ce-f547-40d7-9d87-fe83c0ca6dcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar Bootcamp']/parent::*</value>
      <webElementGuid>7700eaa1-51c9-428e-b46c-fa45f1dd10a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div[2]/button</value>
      <webElementGuid>5b66ffd9-a2e6-484a-8d89-74ee87560a60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                        Daftar Bootcamp
                                    ' or . = '
                                        Daftar Bootcamp
                                    ')]</value>
      <webElementGuid>527d13f2-7a3c-4642-a2e7-affa09cc1b05</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
