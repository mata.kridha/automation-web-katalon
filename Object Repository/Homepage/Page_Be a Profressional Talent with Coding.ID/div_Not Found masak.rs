<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Not Found masak</name>
   <tag></tag>
   <elementGuidId>89deca97-7451-4839-b524-bd8a7c4e6a76</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[10]/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.container-other > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1fc89f2c-aa02-4f0c-a68a-94d0faa11126</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Not Found &quot;masak&quot;</value>
      <webElementGuid>e40bca5d-8252-4a85-a5c4-e4b01244bb40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;skill container-full-search&quot;]/div[@class=&quot;skill container-show-dropdown&quot;]/div[@class=&quot;container-other&quot;]/div[1]</value>
      <webElementGuid>61951446-43b8-416f-93f1-4015e51b4620</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[10]/div/div/div</value>
      <webElementGuid>d9d3fb41-f176-451d-8cfc-0d9fed5eec27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indeed'])[2]/following::div[5]</value>
      <webElementGuid>a5bd2e49-1d79-4825-b7fe-68df7c0822e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Load more'])[2]/following::div[9]</value>
      <webElementGuid>bb48022c-3b0c-4563-a5ee-9fd4ff671430</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Coding.id Update'])[1]/preceding::div[2]</value>
      <webElementGuid>69e7a776-eb29-4cec-ac4d-ea0d3fda4e32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Not Found &quot;']/parent::*</value>
      <webElementGuid>533c5973-f5e1-42cf-8ff3-ef107e35f379</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div[10]/div/div/div</value>
      <webElementGuid>972aa7fe-c58a-4437-a383-33558c376a64</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Not Found &quot;masak&quot;' or . = 'Not Found &quot;masak&quot;')]</value>
      <webElementGuid>3e541619-d1fd-4e81-9b4e-b88d2a4409cd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
