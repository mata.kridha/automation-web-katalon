<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h6_Advance Level</name>
   <tag></tag>
   <elementGuidId>f15777cd-360b-4dbf-a25b-907203982414</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div/div/div/h6</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.blockLevel > h6.new_body2_regular</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
      <webElementGuid>07f6d0c3-a22e-4925-b847-4f7b582f4218</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body2_regular</value>
      <webElementGuid>e2704f5f-e13d-4129-bde7-471a20046ed0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        Advance Level</value>
      <webElementGuid>c08222ec-f7fc-4d8c-b5bc-f50e9bf4be11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;bootcampBlock&quot;]/div[@class=&quot;blockBootcamp&quot;]/ul[@class=&quot;listBootcamp&quot;]/li[@class=&quot;listBootcampItem&quot;]/div[@class=&quot;cardBootcamp&quot;]/div[@class=&quot;containerCardBootcamp&quot;]/div[@class=&quot;blockLevel&quot;]/h6[@class=&quot;new_body2_regular&quot;]</value>
      <webElementGuid>d5bd4551-0f14-406c-a399-d7276629eb3a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div/div/div/h6</value>
      <webElementGuid>d0f5eed6-513a-4e46-8bbb-eb64c2a532ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[3]/following::h6[1]</value>
      <webElementGuid>cb7ef659-a936-4f11-8d3b-7d151c6ce91f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BOOTCAMP'])[1]/following::h6[2]</value>
      <webElementGuid>c648dd5d-0918-4c52-a139-456e74e33305</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[4]/preceding::h6[1]</value>
      <webElementGuid>c10a6997-309b-43b3-8529-5129274a1130</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Materi Belajar'])[1]/preceding::h6[1]</value>
      <webElementGuid>34c88cbb-bead-49f8-94eb-84059cf970e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/div/div/h6</value>
      <webElementGuid>f53ad0c6-1678-4848-bb8f-c9be5f6d21c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h6[(text() = '
                                        Advance Level' or . = '
                                        Advance Level')]</value>
      <webElementGuid>86a31579-d55c-419d-a7ef-a4b44e29aff3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
