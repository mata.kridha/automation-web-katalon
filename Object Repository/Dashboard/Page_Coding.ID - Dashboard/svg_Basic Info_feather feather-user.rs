<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Basic Info_feather feather-user</name>
   <tag></tag>
   <elementGuidId>8012a27c-2d0a-4a2a-8057-dc7c0025340b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Info'])[1]/following::*[name()='svg'][1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>svg.feather.feather-user</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>62a9ed3f-8166-42f3-abc3-9e09833da021</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>5812a477-4091-427e-89d1-67a029b135d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>24</value>
      <webElementGuid>5d5817d2-a5a4-438f-af02-dd671d5f06fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>24</value>
      <webElementGuid>7f49a2a1-75b2-437c-a670-e5e9ff40dd47</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>20fc0df6-ea4e-4ea7-8bb1-55337059d831</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>fill</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>1e1cbbf7-4437-4c72-b2c9-b190faa0cecc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>stroke</name>
      <type>Main</type>
      <value>currentColor</value>
      <webElementGuid>0395d0cd-1bf7-4eaa-96db-ef9832c95c2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>stroke-width</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>5ce41657-a58c-4306-8733-dca2c159c28d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>stroke-linecap</name>
      <type>Main</type>
      <value>round</value>
      <webElementGuid>df4f0046-b945-4a04-8f5d-3eaa458ffe04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>stroke-linejoin</name>
      <type>Main</type>
      <value>round</value>
      <webElementGuid>9c7e5a48-ce3f-4868-9eb7-1bc1b6d5642c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>feather feather-user</value>
      <webElementGuid>cc15d540-9c62-44e1-951b-eae9293175dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sidebar-wrapper&quot;)/ul[@class=&quot;sidebar-menu&quot;]/li[@class=&quot;dropdown&quot;]/a[@class=&quot;nav-link&quot;]/svg[@class=&quot;feather feather-user&quot;]</value>
      <webElementGuid>2a3d6c38-2d03-44f1-af86-ac59256efc38</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Info'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>d7b877bc-27db-4616-a170-21f228cf85e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>03f9a8ad-7f68-4ee5-9379-ec760b1c9755</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>6ff6b744-8aa9-4ddf-a102-5a4299b28923</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing Account'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>bc7dd096-f479-4cc0-ac97-8d65f7cbb03c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
