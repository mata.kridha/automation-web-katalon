<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Points</name>
   <tag></tag>
   <elementGuidId>5848207f-24b1-455b-b71a-879cb34420ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/section/div/div/div/div[2]/h5</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h5.new_body1_semibold</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>d417b010-cfc0-49b1-bc68-28fdd1d38878</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body1_semibold</value>
      <webElementGuid>28964b84-0c5e-4ea2-9789-4eecf273b717</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Points</value>
      <webElementGuid>8b8125e4-6c94-4f4f-a145-4684ce7a4932</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;container-card&quot;]/div[@class=&quot;card-point&quot;]/div[@class=&quot;card-header-point&quot;]/div[@class=&quot;card-right&quot;]/h5[@class=&quot;new_body1_semibold&quot;]</value>
      <webElementGuid>53762e92-1735-407a-a05c-8e46c6f93348</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/section/div/div/div/div[2]/h5</value>
      <webElementGuid>8a6122d4-ca7f-4244-bd94-2ba5910795d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::h5[2]</value>
      <webElementGuid>34fedebc-40c4-483a-abcc-a15ae4e6f023</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='content_copy'])[1]/preceding::h5[1]</value>
      <webElementGuid>2020d0e1-d80d-4e28-aea0-58a50bc8a902</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Points']/parent::*</value>
      <webElementGuid>55983ed4-bbb5-4ca0-b73d-d80bcade519d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/h5</value>
      <webElementGuid>c9f2dbba-89b3-4a0b-9913-ac674c3d3bb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Points' or . = 'Points')]</value>
      <webElementGuid>de697620-954d-4641-bad6-ca2d03fd191a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
