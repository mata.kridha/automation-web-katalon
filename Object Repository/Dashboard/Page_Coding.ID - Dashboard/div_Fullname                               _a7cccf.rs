<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Fullname                               _a7cccf</name>
   <tag></tag>
   <elementGuidId>8e65d3f0-21f8-444d-b56e-b899264597e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-body</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ce3b9391-79e4-4e5a-91e7-b5c0f5ed9103</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body</value>
      <webElementGuid>243a1fb3-98db-41b0-b839-8336be159ec5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        </value>
      <webElementGuid>12809bf1-e258-41d0-a406-8c071ac8d6ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-6&quot;]/div[@class=&quot;card-body&quot;]</value>
      <webElementGuid>ac677dc7-b1d0-440d-85ff-e784de3328f7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div</value>
      <webElementGuid>eeaa5d7c-b91d-4022-baf8-7a7582f4b232</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::div[8]</value>
      <webElementGuid>67d7f5e1-c724-46f5-aee9-f5c0eebd8761</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/div/div/div/div/div/div</value>
      <webElementGuid>a591b2e0-d1d7-4a5b-b85d-ac2850ae02e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        ' or . = '
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        ')]</value>
      <webElementGuid>03e1ae01-165d-4549-8a26-5af3e4f5810c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
