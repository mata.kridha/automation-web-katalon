<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_BirthDay_birth_date</name>
   <tag></tag>
   <elementGuidId>8571065c-2d21-4d28-a0fd-ab9058655220</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;birth_date&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='birth_date']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>7df694d3-4f57-4c8d-93a6-2e4618f845b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>1d9cd5ed-3587-4499-a2fa-2aa8218cf9f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>birth_date</value>
      <webElementGuid>0323b3fe-c487-4001-acfb-aca22b5c530d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control tanggal_lahir </value>
      <webElementGuid>d15538bf-eb43-4d33-ac92-22d92d2dd043</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>19-May-1995</value>
      <webElementGuid>f7d53e48-8911-4561-b298-fe56a06550db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-6&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;py-4&quot;]/div[@class=&quot;form-group&quot;]/input[@class=&quot;form-control tanggal_lahir&quot;]</value>
      <webElementGuid>5b7a2b79-21bd-4806-95a4-70db8d2a9e8a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='birth_date']</value>
      <webElementGuid>86959f5c-6492-432a-9f14-a26a4f719008</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/form/div[2]/div[4]/input</value>
      <webElementGuid>47611b44-6884-4121-9f37-8e01235e9510</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/input</value>
      <webElementGuid>8a95a5fd-6d03-49be-8025-d9d0d0c208cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'birth_date']</value>
      <webElementGuid>a1ba9001-c475-4b3c-a9aa-d811dca75166</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
