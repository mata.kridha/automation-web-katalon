<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Coding.ID</name>
   <tag></tag>
   <elementGuidId>59659c1c-80e5-41a6-9db3-0a285a36f961</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/footer/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.footer-left > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>2f45d6fa-95eb-4881-9d11-10fd537a6ec1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>coding.id</value>
      <webElementGuid>0575cec3-7264-4159-999b-f6751afde2da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Coding.ID</value>
      <webElementGuid>26a19fb8-2638-478c-8cb3-0196284f4419</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/footer[@class=&quot;main-footer&quot;]/div[@class=&quot;footer-left&quot;]/a[1]</value>
      <webElementGuid>72784c14-ba5e-4fb2-96ee-c4a33385517c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/footer/div/a</value>
      <webElementGuid>ae42d41f-8d91-493a-85e8-260f28b0e648</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Coding.ID')]</value>
      <webElementGuid>5d210285-181e-4b14-971b-e93f34879b5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[2]/following::a[1]</value>
      <webElementGuid>aa38eb28-628d-49b1-b45a-2bfe26034557</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notes'])[1]/following::a[1]</value>
      <webElementGuid>3d85c47c-a26f-4060-a77b-799f6d83a812</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'coding.id')]</value>
      <webElementGuid>57c3fb9e-b9d7-401b-8dab-04dfc6a5e78f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//footer/div/a</value>
      <webElementGuid>d6a71a92-8bc1-4423-a6a9-2b2a79bd2b80</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'coding.id' and (text() = 'Coding.ID' or . = 'Coding.ID')]</value>
      <webElementGuid>7b3c5c05-7d36-4812-ac89-d5f52cf76bc3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
