<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Email Already taken</name>
   <tag></tag>
   <elementGuidId>7e2ab96c-e7fe-44d4-b9ec-a11d93200df4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal-content > div.modal-body</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='modalRespon']/div/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f91d7f4a-5c7c-4ba4-967d-82c38fbab3b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-body</value>
      <webElementGuid>775b6ea4-99d1-4743-8b7e-8557c56044cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Email Already taken.
            </value>
      <webElementGuid>e6428ecc-832b-442f-99ee-b9c975add7bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalRespon&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]</value>
      <webElementGuid>9fafd39b-b198-4fbc-ad13-24abfc0bd80d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modalRespon']/div/div/div[2]</value>
      <webElementGuid>099e42eb-23c3-4685-a3ea-1a5dd78ecf4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Info'])[1]/following::div[1]</value>
      <webElementGuid>8d26db5f-c5e8-4685-8db9-2bc504a79c51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/preceding::div[1]</value>
      <webElementGuid>bd5e44aa-0709-4e39-b811-b2637a3b6808</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About CODING.ID'])[1]/preceding::div[2]</value>
      <webElementGuid>c842a8d8-e9ec-4e38-9f2a-2c72f79f39a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div[2]</value>
      <webElementGuid>ee73cc6d-4936-48dd-9e28-5b1b038dcc97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Email Already taken.
            ' or . = '
                Email Already taken.
            ')]</value>
      <webElementGuid>2fdca16d-139d-4cf6-b903-79e883f02882</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
