<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_No available course  masak</name>
   <tag></tag>
   <elementGuidId>7f30bba3-31b7-4a67-a81c-acc0c6ce0ecb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#containerEventInner > h3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='containerEventInner']/h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>b941a807-7c7a-48ef-9eb0-597544fc02dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>No available course &quot; masak &quot;</value>
      <webElementGuid>4ad451b5-94a3-4eba-b8c1-79c51ba76da4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;containerEventInner&quot;)/h3[1]</value>
      <webElementGuid>8f801b72-a31f-4245-94e6-62c4c30dbe47</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='containerEventInner']/h3</value>
      <webElementGuid>816fede6-fb1c-4a83-8c1d-ef0ad27b1619</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Programs'])[1]/following::h3[1]</value>
      <webElementGuid>be743ba8-ed46-4dbc-b760-105f82389431</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING.ID Programs'])[1]/following::h3[1]</value>
      <webElementGuid>acc2ed80-b8d4-47bb-aff3-c6b8932416b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gabung CODING.ID Newsletter'])[1]/preceding::h3[1]</value>
      <webElementGuid>eb56c4c1-928f-4abc-b2a9-17b9288cbc9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Format email tidak valid'])[1]/preceding::h3[1]</value>
      <webElementGuid>c0a3c8fb-3fec-403b-8f8b-b81e1fa14c6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='No available course &quot; masak &quot;']/parent::*</value>
      <webElementGuid>ea16115f-8403-4ea2-a307-3a32d888457b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/div/h3</value>
      <webElementGuid>68ce8d5b-bb5d-4fa8-8bb3-388855506a6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'No available course &quot; masak &quot;' or . = 'No available course &quot; masak &quot;')]</value>
      <webElementGuid>8a4fb5dd-e153-4cfe-a204-baaf3d54d92f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
