<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h6_Lihat Program</name>
   <tag></tag>
   <elementGuidId>64c6182d-3e20-4536-be35-b05abccaa109</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h6.titleProgram</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='classRow']/div/div[2]/a/h6</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
      <webElementGuid>47b5e4a6-7472-4c48-96f6-626a6747aeec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>titleProgram</value>
      <webElementGuid>49e63036-8f2e-4138-8979-b8236f45b8a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    Lihat Program 
                                </value>
      <webElementGuid>2bbd990a-ac13-44c2-b0cd-aff00140ef9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;containerEvent&quot;)/div[1]/ul[@id=&quot;classRow&quot;]/li[@id=&quot;classRow&quot;]/div[1]/div[2]/a[1]/h6[@class=&quot;titleProgram&quot;]</value>
      <webElementGuid>ecf8f940-ee88-4899-a0a5-93a1912ec332</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='classRow']/div/div[2]/a/h6</value>
      <webElementGuid>6b567ba2-268d-4256-ba4e-e62f680199de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Bea...'])[1]/following::h6[1]</value>
      <webElementGuid>7359e37d-90ea-418f-a96b-8386ee843059</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[2]/following::h6[2]</value>
      <webElementGuid>bfe1f9cd-9f2b-4a43-9238-d04c48378304</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fullstack Engineer Class'])[2]/preceding::h6[1]</value>
      <webElementGuid>d776e939-bc3e-4366-8f62-6a55d7382bdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja ...'])[1]/preceding::h6[2]</value>
      <webElementGuid>40f42012-e352-4632-a51f-c66b8e62ffa5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lihat Program']/parent::*</value>
      <webElementGuid>214acf59-9707-4473-b19a-501bbbb37de0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/h6</value>
      <webElementGuid>9bcdb8e4-c2c4-4982-8102-1cd4dd1c7f46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h6[(text() = '
                                    Lihat Program 
                                ' or . = '
                                    Lihat Program 
                                ')]</value>
      <webElementGuid>a03684b4-587b-4274-84bb-1a6ce57e111e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
