<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ul_My                                      _cc781d</name>
   <tag></tag>
   <elementGuidId>1921c147-d84d-4109-8dc6-d81113f22093</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[7]/ul</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.wm-dropdown-menu</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
      <webElementGuid>9961a8c0-a3b9-4c00-bd96-5597d8027af0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wm-dropdown-menu</value>
      <webElementGuid>e358645a-71cd-480b-89a5-09e215186b96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    </value>
      <webElementGuid>c87ae8f2-c3e6-42c3-9396-ce2f82cc9f68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;___class_+?26___&quot;]/ul[@class=&quot;wm-dropdown-menu&quot;]</value>
      <webElementGuid>3d0633cf-c4d0-4773-8af3-46e5b3fe21bc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[7]/ul</value>
      <webElementGuid>27602938-b60d-485b-a67a-e8292ea4717b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[1]/following::ul[1]</value>
      <webElementGuid>f0ec4149-d4f4-4f09-bef7-1f0d1e11a03e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blog'])[1]/following::ul[1]</value>
      <webElementGuid>2d6867ec-57b9-4104-a971-5a8666db927c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/ul</value>
      <webElementGuid>f7c41b70-5777-44ee-9db0-982272fc28d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//ul[(text() = '
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    ' or . = '
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    ')]</value>
      <webElementGuid>f33afae8-efd8-4c33-b7f5-be967f5b4ffc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
