<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Bootcamp</name>
   <tag></tag>
   <elementGuidId>4c34d521-64d0-4ce8-8b03-cb96ae7f34d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[2]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li:nth-of-type(2) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ffe573b6-7ea7-4c8b-b58f-1419d4fec135</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/bootcamp</value>
      <webElementGuid>099666bf-08bc-460a-8305-8d5dacf6a3d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bootcamp</value>
      <webElementGuid>df1ffd7f-6332-4c9c-bfcf-0990d5e48bb5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[2]/a[1]</value>
      <webElementGuid>7f0dc848-f2b3-4817-aa65-49b46589f046</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[2]/a</value>
      <webElementGuid>c52c2cc0-d3a1-4c56-8670-b41022910965</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Bootcamp')]</value>
      <webElementGuid>13d5be50-0d34-4782-8625-39ccc33243bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Toggle navigation'])[1]/following::a[1]</value>
      <webElementGuid>74e79e5b-2069-4062-baf6-364de755b5d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course'])[1]/preceding::a[1]</value>
      <webElementGuid>9a8067f7-55e5-4f79-a4e3-3f2cbe85da81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Events'])[1]/preceding::a[2]</value>
      <webElementGuid>93d6f546-73fe-400a-bfba-dd1e2a6c5d6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bootcamp']/parent::*</value>
      <webElementGuid>8517de26-b45a-4447-b08a-4a83cb9cb2b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/bootcamp')]</value>
      <webElementGuid>45295a40-731b-4e5b-af9b-299c2e7393a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>96b908d3-acf8-4404-882d-678c1bbafb66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/bootcamp' and (text() = 'Bootcamp' or . = 'Bootcamp')]</value>
      <webElementGuid>b302730d-09ef-4c9a-9eb1-440223918462</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
