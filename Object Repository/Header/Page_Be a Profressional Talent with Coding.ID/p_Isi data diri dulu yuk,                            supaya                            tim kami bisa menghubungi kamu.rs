<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Isi data diri dulu yuk,                            supaya                            tim kami bisa menghubungi kamu</name>
   <tag></tag>
   <elementGuidId>5e01e79f-8112-4f49-a370-ef53ae0b0a94</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>76211835-5c51-4e82-9ddb-d2b53c24753a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-show</name>
      <type>Main</type>
      <value>!$store.dataForm.toDahsboard</value>
      <webElementGuid>4acc3999-4599-4aff-8646-fa3c98196146</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body1_regular</value>
      <webElementGuid>c1e2892b-5063-4ba9-a80d-8997fe6a1c7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.</value>
      <webElementGuid>90886b39-9dc5-4cd2-ab27-e8be107f9c37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/p[@class=&quot;new_body1_regular&quot;]</value>
      <webElementGuid>a68e8b21-8ad4-4254-a091-5c4ff9ad4754</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/p</value>
      <webElementGuid>7a14f687-d8c3-4f50-a92d-02976ae3e284</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[7]/following::p[1]</value>
      <webElementGuid>a0a3bdd2-8aa0-42c4-b26f-e4896e079cf5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elka Verso'])[2]/following::p[1]</value>
      <webElementGuid>8df55583-ed45-4515-bdf4-1b24d8930d88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[9]/preceding::p[1]</value>
      <webElementGuid>e8f8d4b8-f590-42be-950b-725605bbf5c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div[3]/div/div[3]/p</value>
      <webElementGuid>4e314b57-7792-4888-a8dd-afafa11d5cf4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.' or . = 'Isi data diri dulu yuk,
                            supaya
                            tim kami bisa menghubungi kamu.')]</value>
      <webElementGuid>0e5a5715-3498-4e1e-a315-d7dd245029e2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
