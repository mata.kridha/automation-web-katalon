<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign Up</name>
   <tag></tag>
   <elementGuidId>263edcbe-875b-4dea-9a32-687641682541</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//footer[@id='wm-footer']/div/div/div/aside[2]/div[2]/ul[2]/li[3]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5b7b0f47-65f2-4cc7-b405-ab423d9d375d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>listSiteMap</value>
      <webElementGuid>a03c8b6f-bd26-4d5b-ac11-c75d7c1acf6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/daftar?</value>
      <webElementGuid>80f81224-06d6-4bd7-b28c-f2b655ddb42f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                Sign Up</value>
      <webElementGuid>67be486e-6ae4-498d-865f-a29c25baf175</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wm-footer&quot;)/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/aside[@class=&quot;col-md-4 col-sm-12 col-12&quot;]/div[@class=&quot;row&quot;]/ul[@class=&quot;col-md-6 col-sm-6 col-xs-6&quot;]/li[3]/a[@class=&quot;listSiteMap&quot;]</value>
      <webElementGuid>b8d12338-f0ac-4fcc-adb1-72181d09c583</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//footer[@id='wm-footer']/div/div/div/aside[2]/div[2]/ul[2]/li[3]/a</value>
      <webElementGuid>25ebe321-3390-4623-9124-fe45fda0c451</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Sign Up')]</value>
      <webElementGuid>03e70bcb-3c1c-43bd-a325-a947d7ea1992</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/following::a[1]</value>
      <webElementGuid>ab5bf4c6-a848-4ace-b72f-1d9de7390108</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[4]/following::a[2]</value>
      <webElementGuid>2981359f-f9e6-411e-8878-617001100559</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[5]/preceding::a[1]</value>
      <webElementGuid>39f5e4c9-4220-4fe8-a97b-ebf87670fecd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course'])[6]/preceding::a[6]</value>
      <webElementGuid>5e02db7b-13af-42d0-b4f0-cf0fcb565480</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign Up']/parent::*</value>
      <webElementGuid>e10f9bac-75a2-40f3-b9dc-f528e9d4ca39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/daftar?')]</value>
      <webElementGuid>8f729984-0029-43a6-81fa-abe1691c21a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul[2]/li[3]/a</value>
      <webElementGuid>7c050197-f7c8-49c4-945a-e534c7c7990d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/daftar?' and (text() = '
                                                Sign Up' or . = '
                                                Sign Up')]</value>
      <webElementGuid>4f49c7f8-792e-490a-a917-b9ec557fa6f2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
