<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Join our                                _7825b6</name>
   <tag></tag>
   <elementGuidId>8ce86307-f152-434f-be70-f95e5b2eee11</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='disapear']/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h3.card-title</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>8bfc9781-9360-4e62-b2fa-dfee8129dc81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-title</value>
      <webElementGuid>62b886ac-f369-41e4-8acd-a4d222277036</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Join our
                                            Developer
                                            Bootcamp</value>
      <webElementGuid>08050f91-095f-4ee6-a031-cbd75c6bac39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;card-right&quot;)/div[@id=&quot;disapear&quot;]/h3[@class=&quot;card-title&quot;]</value>
      <webElementGuid>9cc033ae-b2a9-4ecc-83bd-95b1769e194b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='disapear']/h3</value>
      <webElementGuid>435a49a8-dffe-4cf2-8566-2a47aaeebe9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Featured Topic'])[1]/following::h3[1]</value>
      <webElementGuid>852d1d03-cedc-4eb4-b689-bd702b476142</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[3]/following::h3[1]</value>
      <webElementGuid>953a366e-3ad2-4c04-a9a5-bf8fa2e7294a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advance Level'])[1]/preceding::h3[1]</value>
      <webElementGuid>30acfb9d-ca6c-4484-988e-256bb7f21779</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advance Level'])[2]/preceding::h3[1]</value>
      <webElementGuid>95c7641c-6386-4d96-83e2-bda8f354b843</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>2516b811-4294-434f-ade6-461893b3ebe6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Join our
                                            Developer
                                            Bootcamp' or . = 'Join our
                                            Developer
                                            Bootcamp')]</value>
      <webElementGuid>9807f700-13fa-49d3-86b1-6afa842e903f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
