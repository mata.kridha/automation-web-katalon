<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Site Map</name>
   <tag></tag>
   <elementGuidId>c54fc01a-6d71-4581-8599-3f04dbadbdaf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//footer[@id='wm-footer']/div/div/div/aside[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8d6c4fe1-b640-42bb-96a7-ab84a9650975</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wm-footer-widget-title</value>
      <webElementGuid>9b0aa5f1-07db-409b-9497-11c9888828c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    Site Map
                                </value>
      <webElementGuid>718e8400-8ece-4843-81f9-c5cd8b3df9b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wm-footer&quot;)/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/aside[@class=&quot;col-md-4 col-sm-12 col-12&quot;]/div[@class=&quot;wm-footer-widget-title&quot;]</value>
      <webElementGuid>4ebe6870-49cc-4812-98f6-27e3caf28861</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//footer[@id='wm-footer']/div/div/div/aside[2]/div</value>
      <webElementGuid>0bb94254-71a0-4163-a8da-332b8e7e73ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About CODING.ID'])[1]/following::div[1]</value>
      <webElementGuid>2b09bbfa-c30c-44b5-b2e1-43fad8934d99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Partner Kami'])[1]/following::div[45]</value>
      <webElementGuid>f6496df6-3733-4f9b-bb26-8af10db9721a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside[2]/div</value>
      <webElementGuid>f347cfac-a2d3-4754-a551-92db1e174c51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                    Site Map
                                ' or . = '
                                    Site Map
                                ')]</value>
      <webElementGuid>26d9686b-5693-4765-982e-3bb3e41020d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
