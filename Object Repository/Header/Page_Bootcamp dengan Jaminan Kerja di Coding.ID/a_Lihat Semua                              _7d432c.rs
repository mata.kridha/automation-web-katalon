<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Lihat Semua                              _7d432c</name>
   <tag></tag>
   <elementGuidId>55e8bb01-83d8-49bd-a747-269fba96b669</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='card-right']/div[2]/a[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-course.btn-block</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>9fb6194f-57cb-4945-a402-bc389763bed5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-course btn-block</value>
      <webElementGuid>7a1fc5f4-2e89-4b28-9ff6-7aa90c9a9bc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://demo-app.online/course</value>
      <webElementGuid>80879be4-7c2b-474d-b2ee-18e5b9d0808a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lihat Semua
                                            Online Course ></value>
      <webElementGuid>a2e23c94-3100-4c81-8741-421e46f615d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;card-right&quot;)/div[@class=&quot;card-body&quot;]/a[@class=&quot;btn btn-course btn-block&quot;]</value>
      <webElementGuid>a19f8ba6-b286-461e-84d3-6e3168587d8d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='card-right']/div[2]/a[2]</value>
      <webElementGuid>7930e401-54e9-4710-a460-a5407e27cd3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Lihat Semua
                                            Online Course >')]</value>
      <webElementGuid>67bb950a-4fb1-4ec5-925c-e41e067295ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advance Level'])[2]/following::a[2]</value>
      <webElementGuid>3a3e1fd3-0672-4e21-a613-f5d7b1e92487</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING.ID Programs'])[1]/preceding::a[2]</value>
      <webElementGuid>02080b91-3e93-4216-8690-c6d4d36b7dfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://demo-app.online/course')]</value>
      <webElementGuid>2dd39ec0-32db-4eb8-90b9-22a7ab2ddb95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>5e6816f0-9e70-4610-b639-a6c9f8c692bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://demo-app.online/course' and (text() = 'Lihat Semua
                                            Online Course >' or . = 'Lihat Semua
                                            Online Course >')]</value>
      <webElementGuid>15476c7a-f9a3-4722-887e-e81b250bc852</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
