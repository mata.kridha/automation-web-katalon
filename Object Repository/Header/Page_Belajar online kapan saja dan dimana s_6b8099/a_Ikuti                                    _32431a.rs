<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Ikuti                                    _32431a</name>
   <tag></tag>
   <elementGuidId>9ea53dc4-b4d0-4ee7-abc0-34dc9af08572</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='card-right']/div[2]/a[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-event.btn-block</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3c9b0c16-077b-475c-8439-09db8ca1453a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-event btn-block</value>
      <webElementGuid>d7fde7b8-755d-4cd2-afd5-dd9cb651fdb4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://demo-app.online/event</value>
      <webElementGuid>d8dfc5e7-cdf0-467a-98c0-023d0835903a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ikuti
                                            Events ></value>
      <webElementGuid>05d9734a-d286-4d1c-8147-427e5132a264</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;card-right&quot;)/div[@class=&quot;card-body&quot;]/a[@class=&quot;btn btn-event btn-block&quot;]</value>
      <webElementGuid>53451d41-49a8-4af1-94e7-f404e6535617</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='card-right']/div[2]/a[3]</value>
      <webElementGuid>baa0868e-73c7-4ba4-b474-eae022f86eda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Ikuti
                                            Events >')]</value>
      <webElementGuid>de5504a6-9d53-476c-9ac3-12a14dacc0d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Online Courses'])[1]/preceding::a[1]</value>
      <webElementGuid>7e306bdb-694a-4cc2-91d1-75ba943c0610</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas Terbaru'])[1]/preceding::a[1]</value>
      <webElementGuid>8939b235-fdd5-4170-8d4a-827ad5551a38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://demo-app.online/event')]</value>
      <webElementGuid>1c366679-794b-4f24-9a91-7e63002ba003</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[3]</value>
      <webElementGuid>b8620e7d-801b-46cf-86e5-88b08bc299a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://demo-app.online/event' and (text() = 'Ikuti
                                            Events >' or . = 'Ikuti
                                            Events >')]</value>
      <webElementGuid>1698b94a-eda2-4ef5-b35c-efe961e51617</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
