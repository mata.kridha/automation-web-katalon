<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign-in With Google</name>
   <tag></tag>
   <elementGuidId>40cf9c8f-53d9-4b13-8e5b-72481c9c4619</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonGoogleTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonGoogleTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>ab0e1814-f3dc-49bd-8c0e-cbafd49b2997</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonGoogleTrack</value>
      <webElementGuid>f15c86a0-a383-4d35-babd-9c9ac5dcef96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9b644a4f-8523-4638-ab46-ff0feb30f599</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn wm-all-events btn-block bg-white</value>
      <webElementGuid>930d2a54-2b4e-4a1b-b90c-11c994cb732c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                            
                                                            Sign-in With Google
                                                        </value>
      <webElementGuid>aedd00a1-8057-42ec-b748-1048b6540751</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonGoogleTrack&quot;)</value>
      <webElementGuid>72d468d3-aef5-45be-ae7b-3e3d5c1b900c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonGoogleTrack']</value>
      <webElementGuid>2e7be05e-a43e-47f9-b20b-b5c3b42dbfb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/following::button[1]</value>
      <webElementGuid>34dff0ef-97b9-4985-9655-272f2bcd9637</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa kata sandi ?'])[1]/preceding::button[1]</value>
      <webElementGuid>dba8f1ac-8552-4460-a57f-cade2112f6e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign-in With Google']/parent::*</value>
      <webElementGuid>0d4046a4-c118-4699-ab27-55d9e1d40c4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/button</value>
      <webElementGuid>d3b2f1da-90f5-43f8-a939-4d0f19eb8bbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonGoogleTrack' and @type = 'button' and (text() = '
                                                            
                                                            Sign-in With Google
                                                        ' or . = '
                                                            
                                                            Sign-in With Google
                                                        ')]</value>
      <webElementGuid>c3f45ea1-2afa-43f4-815f-4de94bdb4c0c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
