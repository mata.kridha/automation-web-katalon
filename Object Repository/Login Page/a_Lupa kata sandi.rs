<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Lupa kata sandi</name>
   <tag></tag>
   <elementGuidId>009e77d7-eb6b-48e2-b53c-f24b49fca5be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='buttonForgetPassTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonForgetPassTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>65874f99-dc80-4880-8b77-1727a598f903</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonForgetPassTrack</value>
      <webElementGuid>337a74f1-1c6b-413c-a3b5-df78e5c91531</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://demo-app.online/password/reset</value>
      <webElementGuid>1457a0a4-4a54-481a-baab-9873fd2ec0ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lupa kata sandi ?</value>
      <webElementGuid>48082589-3b3c-4e17-98a6-9fa90e69c86e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonForgetPassTrack&quot;)</value>
      <webElementGuid>ff87fef2-cbe6-46a0-b03f-d6b9fb45902d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='buttonForgetPassTrack']</value>
      <webElementGuid>d8a42ab6-06bc-4f01-9820-b07679b0303f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Lupa kata sandi ?')]</value>
      <webElementGuid>53c46ede-4d99-4290-bfc1-47cefb20928d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/following::a[2]</value>
      <webElementGuid>a79f5fdf-3d70-4375-8a82-730d1a3d0908</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat akun'])[1]/preceding::a[1]</value>
      <webElementGuid>113e91a2-2e79-41ed-afbf-5cb76b434df6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lupa kata sandi ?']/parent::*</value>
      <webElementGuid>48ef4f99-2f5c-4d0a-a2e7-7c62d8dc9909</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://demo-app.online/password/reset')]</value>
      <webElementGuid>1ee52bad-5b80-4002-93cf-f30ae1733500</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div/div/div/div/a</value>
      <webElementGuid>ffd9d5e7-5fc6-4b3a-b703-18438f2b3607</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'buttonForgetPassTrack' and @href = 'https://demo-app.online/password/reset' and (text() = 'Lupa kata sandi ?' or . = 'Lupa kata sandi ?')]</value>
      <webElementGuid>e918fc5d-6d57-4ef7-a6ce-012e5d4c1e3b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
