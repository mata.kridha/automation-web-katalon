import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.mouseOver(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/input_Toggle navigation_search-bar'))

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/input_Toggle navigation_search-bar'))

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/p_Quality Assurance Engineer Class'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Quality Assurance Engineer Class - Cod_d9a6f3/h1_Quality Assurance Engineer Class'), 
    'Quality Assurance Engineer Class')

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/input_Toggle navigation_search-bar'))

WebUI.click(findTestObject('Object Repository/Header/Page_Quality Assurance Engineer Class - Cod_d9a6f3/p_Fullstack Engineer Class'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Fullstack Engineer Class - Coding.ID Bootcamp/h1_Fullstack Engineer Class'), 
    'Fullstack Engineer Class')

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/input_Toggle navigation_search-bar'))

WebUI.click(findTestObject('Object Repository/Header/Page_Fullstack Engineer Class - Coding.ID Bootcamp/a_Lihat Semua                              _756db5'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/h1_CODING.ID Programs'), 
    'CODING.ID Programs')

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/input_Toggle navigation_search-bar'))

WebUI.click(findTestObject('Object Repository/Header/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/a_Lihat Semua                              _7d432c'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Belajar online kapan saja dan dimana s_6b8099/h1_Online Courses'), 
    'Online Courses')

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/input_Toggle navigation_search-bar'))

WebUI.click(findTestObject('Object Repository/Header/Page_Belajar online kapan saja dan dimana s_6b8099/a_Ikuti                                    _32431a'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Online event bersertifikat dari prakti_f42b96/h1_CODING.ID Event'), 
    'CODING.ID Event')

WebUI.closeBrowser()

