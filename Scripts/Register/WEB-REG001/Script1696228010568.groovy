import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.github.javafaker.Faker

//Faker faker = new Faker();

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/daftar?')

WebUI.setText(findTestObject('Register/input_Nama'), '')

WebUI.setText(findTestObject('Register/input_E-Mail'), '')

WebUI.setText(findTestObject('Register/input_TTL'), '')

WebUI.setText(findTestObject('Register/input_Whatsapp'), '')

WebUI.setText(findTestObject('Register/input_Kata Sandi'), '')

WebUI.setText(findTestObject('Register/input_Konfirmasi'), '')

WebUI.click(findTestObject('Register/checkbox_Setuju'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Register/btn_Daftar'), FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

