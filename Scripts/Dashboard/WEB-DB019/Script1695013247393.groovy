import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'team2.helper.preconLogin2'()

WebUI.navigateToUrl('https://demo-app.online/dashboard')

WebUI.click(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/a_Notifications_nav-link nav-link-lgcollapse-btn'))

WebUI.mouseOver(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/rect'))

WebUI.mouseOver(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/svg_Basic Info_feather feather-user'))

WebUI.mouseOver(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/rect_1'))

WebUI.mouseOver(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/rect_1_2'))

WebUI.mouseOver(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/svg_My                                     _cef268'))

WebUI.mouseOver(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/a_My                                        Points'))

WebUI.verifyElementText(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/li_My                                      _c5d412'), 
    'MY POINTS')

WebUI.takeScreenshot()

WebUI.mouseOver(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/a_Invoice'))

WebUI.closeBrowser()

