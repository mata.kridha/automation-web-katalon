import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

TestData nData = findTestData('Data Files/ddt-Profile-Pic')

CustomKeywords.'team2.helper.preconLogin'()

WebUI.navigateToUrl('https://demo-app.online/dashboard/profile')

for (int rowData = 1; rowData <= nData.getRowNumbers(); rowData++) {
    WebUI.click(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/a_Edit Profile'))

    WebUI.uploadFile(findTestObject('Dashboard/upload_profilePic'), RunConfiguration.getProjectDir() + nData.getValue(2, 
            rowData), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/input_Fullname_name'),
		nData.getValue(4, rowData), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('Dashboard/input_wa'), nData.getValue(5, 
            rowData), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/input_BirthDay_birth_date (1)'), 
        nData.getValue(3, rowData), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.sendKeys(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/input_BirthDay_birth_date (1)'), 
        Keys.chord(Keys.ENTER))

    WebUI.delay(2)

    WebUI.verifyElementText(findTestObject('Dashboard/verify_change'), 'Berhasil')

    WebUI.click(findTestObject('Dashboard/ok_button'))
}

WebUI.closeBrowser(FailureHandling.CONTINUE_ON_FAILURE)

