import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.click(findTestObject('Object Repository/PLP/Page_Be a Profressional Talent with Coding.ID/a_Bootcamp'))

WebUI.verifyElementText(findTestObject('Object Repository/PLP/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/h2_Programs'), 
    'Programs')

WebUI.mouseOver(findTestObject('Object Repository/PLP/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/h6_Lihat Program'))

WebUI.verifyElementText(findTestObject('Object Repository/PLP/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/h6_Quality Assurance Engineer Class'), 
    'Quality Assurance Engineer Class')

WebUI.click(findTestObject('Object Repository/PLP/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/h6_Lihat Program'))

WebUI.verifyElementText(findTestObject('Object Repository/PLP/Page_Quality Assurance Engineer Class - Cod_d9a6f3/h1_Quality Assurance Engineer Class'), 
    'Quality Assurance Engineer Class')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/PLP/Page_Quality Assurance Engineer Class - Cod_d9a6f3/a_Bootcamp'))

WebUI.mouseOver(findTestObject('Object Repository/PLP/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/h6_Lihat Program'))

WebUI.verifyElementText(findTestObject('Object Repository/PLP/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/h6_Fullstack Engineer Class'), 
    'Fullstack Engineer Class')

WebUI.click(findTestObject('PLP/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/h6_Lihat Program (1)'))

WebUI.verifyElementText(findTestObject('Object Repository/PLP/Page_Fullstack Engineer Class - Coding.ID Bootcamp/h1_Fullstack Engineer Class'), 
    'Fullstack Engineer Class')

WebUI.takeScreenshot()

WebUI.closeBrowser()

