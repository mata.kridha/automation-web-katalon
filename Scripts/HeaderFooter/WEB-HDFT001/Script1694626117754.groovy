import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'team2.helper.preconLogin'()

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/a_Bootcamp'))

WebUI.navigateToUrl('https://demo-app.online/bootcamp')

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/h1_CODING.ID Programs'), 
    'CODING.ID Programs')

WebUI.click(findTestObject('Object Repository/Header/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/img_Toggle navigation_logonya'))

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/a_Course'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Belajar online kapan saja dan dimana s_6b8099/h1_Online Courses'), 
    'Online Courses')

WebUI.click(findTestObject('Object Repository/Header/Page_Belajar online kapan saja dan dimana s_6b8099/img_Toggle navigation_logonya'))

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/a_Events'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Online event bersertifikat dari prakti_f42b96/h1_CODING.ID Event'), 
    'CODING.ID Event')

WebUI.click(findTestObject('Object Repository/Header/Page_Online event bersertifikat dari prakti_f42b96/img_Toggle navigation_logonya'))

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/a_Kontak'))

WebUI.click(findTestObject('Object Repository/Header/Page_Hubungi kami untuk info lebih lanjut -_dfbcce/h1_Kontak Kami'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Hubungi kami untuk info lebih lanjut -_dfbcce/h1_Kontak Kami'), 
    'Kontak Kami')

WebUI.click(findTestObject('Object Repository/Header/Page_Hubungi kami untuk info lebih lanjut -_dfbcce/img_Toggle navigation_logonya'))

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/a_Blog'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Home - Coding.ID Blog/h2_New Post'), 'New Post')

WebUI.takeScreenshot()

WebUI.closeBrowser()

