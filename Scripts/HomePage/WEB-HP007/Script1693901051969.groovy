import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Precondition/precondition'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h6_Quality Assurance Engineer Class (1) (1)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/button_Daftar Bootcamp (1) (1)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h2_Pendaftaran Bootcamp (1) (1)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/button_Selanjutnya (1) (1)'))

WebUI.scrollToElement(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h5_Nama Instansi Pendidikan'), 
    1)

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/button_Selanjutnya'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/button_Ok'))

WebUI.verifyElementText(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Jurusan tidak boleh kosong'), 
    'Jurusan tidak boleh kosong')

WebUI.closeBrowser()

