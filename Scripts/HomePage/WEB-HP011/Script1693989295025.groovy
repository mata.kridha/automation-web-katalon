import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'team2.helper.preconLogin'()

WebUI.mouseOver(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h6_Quality Assurance Engineer Class (1) (1)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/button_Daftar Bootcamp (1) (1)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h2_Pendaftaran Bootcamp (1) (1)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/button_Selanjutnya (1) (1)'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Domisili'), 
    1)

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Domisili'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h5_KAB TIMOR TENGAH SELATAN'))

WebUI.setText(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/input__jurusan-field'), 
    'hahaeehe')

WebUI.scrollToElement(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Instansi'), 
    1)

WebUI.waitForElementClickable(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Instansi'), 
    1)

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Instansi'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h5_Akademi Akuntansi Bandung'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Referensi'), 
    1)

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Referensi'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h5_Indeed'))

WebUI.click(findTestObject('Homepage/Page_Be a Profressional Talent with Coding.ID/button_Selanjutnya (1)'))

WebUI.scrollToElement(findTestObject('Homepage/Page_Be a Profressional Talent with Coding.ID/p_Isi data diri dulu yuk,                  _01691a'), 
    1)

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih skill'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h5_C'))

WebUI.verifyElementText(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_C'), 
    'C#')

WebUI.mouseOver(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_C'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_C'))

WebUI.click(findTestObject('Homepage/Page_Be a Profressional Talent with Coding.ID/h5_C'))

WebUI.verifyElementText(findTestObject('Homepage/Page_Be a Profressional Talent with Coding.ID/div_C'), 'C#')

WebUI.takeScreenshot()

WebUI.closeBrowser()

