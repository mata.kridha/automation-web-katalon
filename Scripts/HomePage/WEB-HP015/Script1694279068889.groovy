import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'team2.helper.preconLogin'()

WebUI.mouseOver(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h6_Quality Assurance Engineer Class (1) (1) (1)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/button_Daftar Bootcamp (1) (1) (1)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h2_Pendaftaran Bootcamp (1) (1) (1)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/button_Selanjutnya (1) (1) (1)'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Domisili (2)'), 
    1)

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Domisili (2)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h5_KAB TIMOR TENGAH SELATAN (1)'))

WebUI.setText(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/input__jurusan-field (2)'), 
    'hahaeehe')

WebUI.scrollToElement(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Instansi (2)'), 
    1)

WebUI.waitForElementClickable(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Instansi (2)'), 
    1)

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Instansi (2)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h5_Akademi Akuntansi Bandung (1)'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Referensi (2)'), 
    1)

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/div_Pilih Referensi (2)'))

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h5_Indeed (2)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/button_Selanjutnya (1) (2)'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h5_Apakah kamu                             _d1f2d0 (2)'), 
    1)

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/input_Ya_store.dataForm.isCanProgram'))

WebUI.setText(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/textarea__Text1 (1)'), 
    'Ada dua tipe orang di dunia ini. Mereka yang mempunyai mimpi besar, dan mereka yang bangun untuk mew')

WebUI.scrollToElement(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/input_Kode Refferal                        _21f1e9 (1)'), 
    1)

WebUI.setText(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/input_Kode Refferal                        _21f1e9 (1)'), 
    '83274892')

WebUI.scrollToElement(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/h5_Kode Refferal                           _8c4a25 (1)'), 
    1)

WebUI.click(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/button_Daftar Bootcamp (2)'))

WebUI.verifyElementText(findTestObject('Object Repository/Homepage/Page_Be a Profressional Talent with Coding.ID/p_Email sudah terdaftar pada kelas tersebut (1)'), 
    'Email sudah terdaftar pada kelas tersebut')

WebUI.takeScreenshot()

WebUI.closeBrowser()

